<?php

/**
 * Created by PhpStorm.
 * User: GEO
 * Date: 19.12.2016
 * Time: 18:06
 */
include_once "KmlParser.php";
include_once  "DB_Connection.php";
class DataUploader
{
    private $probName;
    private $trackFile;
    private $heartFreqFile;
    private $probApp;
    private $pics = array();

    /**
     * DataUploader constructor.
     * @param $probName
     * @param $trackFile
     * @param $heartFreqFile
     * @param $pics
     */
    /*public function __construct($probName,$trackFile,$heartFreqFile,$app,$pics)
    {
        $this->probName=$probName;
        $this->trackFile=$trackFile;
        $this->heartFreqFile=$heartFreqFile;
        $this->probApp = $app;
        $this->pics=$pics;
    }*/

    public function __construct()
    {
        $this->probName=$_POST["uploadProbName"];
        //$this->trackFile=$_POST["uploadTrack"];

    }

    /**
     * @return mixed
     */
    public function getProbName()
    {
        return $this->probName;
    }

    /**
     * @return mixed
     */
    public function getTrackFile()
    {
        return $this->trackFile;
    }

    /**
     * @return mixed
     */
    public function getHeartFreqFile()
    {
        return $this->heartFreqFile;
    }

    /**
     * @return mixed
     */
    public function getProbApp()
    {
        return $this->probApp;
    }

    /**
     * @return array
     */
    public function getPics(): array
    {
        return $this->pics;
    }
    public function saveProband(){
        $pname=$_POST["uploadProbName"];
        $instDB = DB_Connection::getConnectionInstance();
        $query = "INSERT INTO pnames (unic_name) VALUE(?) ";
        $result = $instDB->insertRow($query, [$pname]);
    }

    public function saveTrackFile(){
        $pname = $_POST["uploadProbName"];
        $instDB = DB_Connection::getConnectionInstance();

            $targetDir = "../probandenData/";
            $targetFile = $targetDir . basename($_FILES["uploadTrack"]["name"]);
            $uploadOk = true;
            $kmlFileType = pathinfo($targetFile, PATHINFO_EXTENSION);


            if ($_FILES["uploadTrack"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = false;
            }
        if ($kmlFileType != "kmz") {
                echo "Sorry, only kml. allowed.";
                $uploadOk = false;
            }
            if ($uploadOk == false) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["uploadTrack"]["tmp_name"], $targetFile)) {
                    echo "The file " . basename($_FILES["uploadTrack"]["name"]) .
                        " has been uploaded.";
                    $uploadOk = true;

                    $newProbTrackFile = $targetDir . $_POST["uploadProbName"] . "Track.kml";
                    $targetFile = $this->extractZip($targetFile, $targetDir);
                    usleep(2);
                    $targetFile = $targetDir . basename($targetFile);
                    rename($targetFile, $newProbTrackFile);

                    $parsedKmlTrack = new KmlParse($newProbTrackFile);
                    $trackArr = $parsedKmlTrack->getParsedKmlTrack();
                    foreach ($trackArr as $trackData) {
                        $query = "INSERT INTO ptrack (pname,latitude,longtitude,time)VALUES(?,?,?,?) ";
                        $result = $instDB->insertRow($query, [$pname, $trackData["lat"], $trackData
                        ["lng"], $trackData["time"]]);

                    }


                } else {
                    echo "Sorry, there was an error uploading your file.";
                    $uploadOk = false;
                }
            }

    }

    public function savePicFile()
    {
            $j = 0;
            $targetDir = "../probandenData/images/";
            for ($i = 0; $i < count($_FILES['uploadBilder']['name']); $i++) {
                $uploadOk=true;

                $j = $j + 1;
                $targetFile = $targetDir . basename($_FILES['uploadBilder']['name'][$i]);

                if (move_uploaded_file($_FILES['uploadBilder']['tmp_name'][$i], $targetFile)) {

                    echo "The file " .basename($_FILES["uploadBilder"]["name"][$i]) .
                        " has been uploaded.";

                } else {//if file was not moved.
                    echo "Sorry, there was an error uploading your file.";
                    echo "reason:: ". $_FILES['uploadBilder']['error'];
                }

            }

    }

    public function saveHeartFreqFile(){
        $pname = $_POST["uploadProbName"];
        $instDB = DB_Connection::getConnectionInstance();
        $targetDir = "../probandenData/";
        $targetFile = $targetDir . basename($_FILES["uploadHeartFreq"]["name"]);
        $uploadOk = true;
        $kmlFileType = pathinfo($targetFile, PATHINFO_EXTENSION);


        if ($_FILES["uploadHeartFreq"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = false;
        }
        if ($kmlFileType != "csv") {
            echo "Sorry, only csv allowed.";
            $uploadOk = false;
        }
        if ($uploadOk == false) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["uploadHeartFreq"]["tmp_name"], $targetFile)) {
                echo "The file " . basename($_FILES["uploadHeartFreq"]["name"]) .
                    " has been uploaded.";
                $uploadOk = true;
                $newProbHeartFreqFile =$targetDir. $_POST["uploadProbName"]."Heart.csv";
                rename($targetFile, $newProbHeartFreqFile);
                $csv = array_map('str_getcsv', file($newProbHeartFreqFile));
                $heartBeat = [];
                $i=0;
                foreach ($csv as $item) {
                    $line=explode(";",$item[0]);
                    $heartBeat[$i]=$line[1];
                    $i++;
                }
                $sec = 0;
                foreach ($heartBeat as $freqData) {
                    $query = "INSERT INTO pfreq (pname,freq,time)VALUES(?,?,?) ";
                    $result = $instDB->insertRow($query, [$pname, $freqData, $sec]);
                    $sec = $sec + 4;
                }
            } else {
                echo "Sorry, there was an error uploading your file.";
                $uploadOk = false;
            }
        }

    }

    public function saveAppFile(){

        $pname = $_POST["uploadProbName"];
        $instDB = DB_Connection::getConnectionInstance();

        $targetDir = "../probandenData/";
        $targetFile = $targetDir . basename($_FILES["uploadApp"]["name"]);
        $uploadOk = true;
        $kmlFileType = pathinfo($targetFile, PATHINFO_EXTENSION);


        if ($_FILES["uploadApp"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = false;
        }
        if ($kmlFileType != "csv") {
            echo "Sorry, only csv allowed.";
            $uploadOk = false;
        }
        if ($uploadOk == false) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["uploadApp"]["tmp_name"], $targetFile)) {
                echo "The file " . basename($_FILES["uploadApp"]["name"]) .
                    " has been uploaded.\n";
                $uploadOk = true;
                $pname=$_POST["uploadProbName"];
                $newProbHeartFreqFile =$targetDir.$pname."App.csv";
                rename($targetFile, $newProbHeartFreqFile);
                $file=file($newProbHeartFreqFile);
                $csv = array_map('str_getcsv',$file,array_fill(0, count($file), '\"'));
                $hotSpot=[];
                //column numbers hotspot
                $hotSpotTagColNum=26;$hotSpotColLng=21;$hotSpotColTime=3;
                $hotSpotLikeColNum = 25;
                $hotSpotColLat = 20;
                $hotSpotPicName = 24;
                $hotSpot2TagColNum = 34;
                $hotSpot2ColLng = 29;
                $hotSpot2PicName = 32;
                $hotSpot2LikeColNum=33;$hotSpot2ColLat=28; $hotSpot2=27;
                //column numbers entering exiting
                $zufrieden=36;$unzufrieden=39;$gluecklich=37;$ungluecklich=38;$wach=40;$munter=41;
                $schlapp=42;$muede=43;$gelassen=44;$entspannt=45;$angespannt=46;$nervoes=47;

                $i=0;
                $colNum = 0;
                $rowNum = 0;
                $exitOnce = 0;
                foreach ($csv as $row) {
                    $line=explode(";",$row[0]);
                    $triggerName=$line[1];

                    //row
                    if ($rowNum === 0) {
                        $latCol = $line[19];
                        if (strpos($latCol, 'Hotspot1_Geo_latitude') !== false) {
                            $hotSpotTagColNum--;
                            $hotSpotColLng--;
                            $hotSpotLikeColNum--;
                            $hotSpotColLat--;
                            $hotSpotPicName--;
                            $hotSpot2TagColNum--;
                            $hotSpot2ColLng--;
                            $hotSpot2PicName--;
                            $hotSpot2LikeColNum--;
                            $hotSpot2ColLat--;
                            $hotSpot2--;
                            //column numbers entering exiting
                            $zufrieden--;
                            $unzufrieden--;
                            $gluecklich--;
                            $ungluecklich--;
                            $wach--;
                            $munter--;
                            $schlapp--;
                            $muede--;
                            $gelassen--;
                            $entspannt--;
                            $angespannt--;
                            $nervoes--;
                        }
                    }

                    //garden enter
                    if ($rowNum === 2) {

                        $zufriedenVar = $line[$zufrieden];
                        $zufriedenVar = trim($zufriedenVar, '"');
                        $zufriedenVar = intval($zufriedenVar);
                        $unzufriedenVar = $line[$unzufrieden];
                        $unzufriedenVar = trim($unzufriedenVar, '"');
                        $unzufriedenVar = intval($unzufriedenVar);
                        $gluecklichVar = $line[$gluecklich];
                        $gluecklichVar = trim($gluecklichVar, '"');
                        $gluecklichVar = intval($gluecklichVar);
                        $ungluecklichVar = $line[$ungluecklich];
                        $ungluecklichVar = trim($ungluecklichVar, '"');
                        $ungluecklichVar = intval($ungluecklichVar);
                        $wachVar = $line[$wach];
                        $wachVar = trim($wachVar, '"');
                        $wachVar = intval($wachVar);
                        $munterVar = $line[$munter];
                        $munterVar = trim($munterVar, '"');
                        $munterVar = intval($munterVar);
                        $schlappVar = $line[$schlapp];
                        $schlappVar = trim($schlappVar, '"');
                        $schlappVar = intval($schlappVar);
                        $muedeVar = $line[$muede];
                        $muedeVar = trim($muedeVar, '"');
                        $muedeVar = intval($muedeVar);
                        $gelassenVar = $line[$gelassen];
                        $gelassenVar = trim($gelassenVar, '"');
                        $gelassenVar = intval($gelassenVar);
                        $entspanntVar = $line[$entspannt];
                        $entspanntVar = trim($entspanntVar, '"');
                        $entspanntVar = intval($entspanntVar);
                        $angespanntVar = $line[$angespannt];
                        $angespanntVar = trim($angespanntVar, '"');
                        $angespanntVar = intval($angespanntVar);
                        $nervoesVar = $line[$nervoes];
                        $nervoesVar = trim($nervoesVar, '"');
                        $nervoesVar = intval($nervoesVar);
                        //QUERY
                        $query = "INSERT INTO penter (pname,zufrieden,gluecklich
                        ,ungluecklich,unzufrieden,wach,munter,schlapp,muede,gelassen
                        ,entspannt,angespannt,nervoes)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                        $resultEnter = $instDB->insertRow($query, [$pname, $zufriedenVar
                            , $gluecklichVar, $ungluecklichVar, $unzufriedenVar,
                            $wachVar, $munterVar, $schlappVar, $muedeVar,
                            $gelassenVar, $entspanntVar, $angespanntVar, $nervoesVar]);


                    }

                    //hotspots
                    if (strpos($triggerName,'Hotspot')!==false){
                        $hashtag1 = $line[$hotSpotTagColNum];
                        $like1 = $line[$hotSpotLikeColNum];
                        $hashtag1 = trim($hashtag1, '"');
                        $like1 = trim($like1, '"');
                        $lng1 = $line[$hotSpotColLng];
                        $lat1 = $line[$hotSpotColLat];
                        $lng1 = trim($lng1, '"');
                        $lat1 = trim($lat1, '"');
                        $hotSpotTime1 = $line[$hotSpotColTime];
                        $picName1 = $line[$hotSpotPicName];
                        $hotSpotTime1 = trim($hotSpotTime1, '"');
                        $picName1 = trim($picName1, '"');
                        array_push($hotSpot, array("hashTag" => $hashtag1,
                            "like" => $like1, "lng" => $lng1, "lat" => $lat1,
                            "hotspotTime" => $hotSpotTime1, "picName" => $picName1));

                        $i++;

                        if($line[$hotSpot2]==="\"1\""){
                            $hashtag2 = $line[$hotSpot2TagColNum];
                            $like2 = $line[$hotSpot2LikeColNum];
                            $hashtag2 = trim($hashtag2, '"');
                            $like2 = trim($like2, '"');
                            $lng2 = $line[$hotSpot2ColLng];
                            $lat2 = $line[$hotSpot2ColLat];
                            $lng2 = trim($lng2, '"');
                            $lat2 = trim($lat2, '"');
                            $hotSpotTime2 = $line[$hotSpotColTime];
                            $picName2 = $line[$hotSpot2PicName];
                            $hotSpotTime2 = trim($hotSpotTime1, '"');
                            $picName2 = trim($picName2, '"');
                            array_push($hotSpot, array("hashTag" => $hashtag2,
                                "like" => $like2, "lng" => $lng2, "lat" => $lat2,
                                "hotspotTime" => $hotSpotTime1, "picName" => $picName2));

                            $i++;
                        }

                    }


                    //garden left
                    if (strpos($triggerName, 'was left') !== false) {
                        $exitOnce++;
                        if ($exitOnce === 1) {
                            $zufriedenVar = $line[$zufrieden];
                            $zufriedenVar = trim($zufriedenVar, '"');
                            $zufriedenVar = intval($zufriedenVar);
                            $unzufriedenVar = $line[$unzufrieden];
                            $unzufriedenVar = trim($unzufriedenVar, '"');
                            $unzufriedenVar = intval($unzufriedenVar);
                            $gluecklichVar = $line[$gluecklich];
                            $gluecklichVar = trim($gluecklichVar, '"');
                            $gluecklichVar = intval($gluecklichVar);
                            $ungluecklichVar = $line[$ungluecklich];
                            $ungluecklichVar = trim($ungluecklichVar, '"');
                            $ungluecklichVar = intval($ungluecklichVar);
                            $wachVar = $line[$wach];
                            $wachVar = trim($wachVar, '"');
                            $wachVar = intval($wachVar);
                            $munterVar = $line[$munter];
                            $munterVar = trim($munterVar, '"');
                            $munterVar = intval($munterVar);
                            $schlappVar = $line[$schlapp];
                            $schlappVar = trim($schlappVar, '"');
                            $schlappVar = intval($schlappVar);
                            $muedeVar = $line[$muede];
                            $muedeVar = trim($muedeVar, '"');
                            $muedeVar = intval($muedeVar);
                            $gelassenVar = $line[$gelassen];
                            $gelassenVar = trim($gelassenVar, '"');
                            $gelassenVar = intval($gelassenVar);
                            $entspanntVar = $line[$entspannt];
                            $entspanntVar = trim($entspanntVar, '"');
                            $entspanntVar = intval($entspanntVar);
                            $angespanntVar = $line[$angespannt];
                            $angespanntVar = trim($angespanntVar, '"');
                            $angespanntVar = intval($angespanntVar);
                            $nervoesVar = $line[$nervoes];
                            $nervoesVar = trim($nervoesVar, '"');
                            $nervoesVar = intval($nervoesVar);
                            if ($nervoesVar === 0 && $angespanntVar === 0 && $gelassenVar === 0
                                && $gluecklichVar === 0
                            ) {
                                $exitOnce--;
                                continue;
                            }
                            //QUERY
                            $query = "INSERT INTO pleft (pname,zufrieden,gluecklich
                            ,ungluecklich,unzufrieden,wach,munter,schlapp,muede,gelassen
                            ,entspannt,angespannt,nervoes)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                            $resultEnter = $instDB->insertRow($query, [$pname, $zufriedenVar
                                , $gluecklichVar, $ungluecklichVar, $unzufriedenVar,
                                $wachVar, $munterVar, $schlappVar, $muedeVar,
                                $gelassenVar, $entspanntVar, $angespanntVar, $nervoesVar]);
                        }
                    }
                    $rowNum++;

                }

                foreach ($hotSpot as $hotspotData) {
                    $query = "INSERT INTO papp (pname,bild_name,latitude,longtitude
                    ,hotspot_bewertung,hotspot_name,time)VALUES(?,?,?,?,?,?,?) ";
                    $resultHotspot = $instDB->insertRow($query, [$pname, $hotspotData["picName"], $hotspotData
                    ["lat"], $hotspotData["lng"], $hotspotData["like"], $hotspotData["hashTag"]
                        , $hotspotData["hotspotTime"]]);

                }

            } else {
                echo "Sorry, there was an error uploading your file.";
                $uploadOk = false;
            }
        }


    }

    function extractZip($file, $target)
    {

        // Creating new ZipArchive object
        $zip = new ZipArchive();

        // Opening the file
        $open = $zip->open($file);

        //Checking if file has been opened properly
        if ($open === true) {

            // Extracting the zip file
            $zip->extractTo($target);
            $unzippedFile = $zip->getNameIndex(0);
            //Closing the zip file
            $zip->close();

            // Deleting the zip file
            unlink($file);

            return $unzippedFile;
        } else {
            return "NULL";
        }

    }

}