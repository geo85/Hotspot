<?php
include_once "HotSpotLogic.php";
function getEnterData()
{
    $logicInst = new HotSpotLogic();
    $enterData = $logicInst->getAppEnter();

    echo json_encode($enterData);

}

function getExitData()
{
    $logicInst = new HotSpotLogic();
    $exitData = $logicInst->getAppExit();


    echo json_encode($exitData);

}

function getAllEnterData()
{
    $logicInst = new HotSpotLogic();
    $enterAllData = $logicInst->getAllAppEnter();

    echo json_encode($enterAllData);
}

function getAllExitData()
{

    $logicInst = new HotSpotLogic();
    $exitAllData = $logicInst->getAllAppExit();

    echo json_encode($exitAllData);
}

function getHeartExitData()
{
    $logicInst = new HotSpotLogic();
    $exitHeartData = $logicInst->getAllHeartExitData();

    echo json_encode($exitHeartData);

}

function getHeartEnterData()
{
    $logicInst = new HotSpotLogic();
    $exitHeartData = $logicInst->getAllHeartEnterData();

    echo json_encode($exitHeartData);

}





if (isset($_POST['pexit'])) {
    getExitData();
}

if (isset($_POST['penter'])) {
    getEnterData();
}
if (isset($_POST['pexitAll'])) {
    getAllExitData();
}

if (isset($_POST['penterAll'])) {
    getAllEnterData();
}

if (isset($_POST['pexitHeart'])) {
    getHeartExitData();
}

if (isset($_POST['penterHeart'])) {
    getHeartEnterData();
}
