<?php

class DB_Connection
{

    private $HOST = "localhost"; // define host
    private $USER = "root";         // define user
    private $PASS = "";             // define pass if is set
    private $DATABASE = "hotspot";     // define database name

    public $isConnected; // neccessary to check if connection is there
    protected $database; //
    private static $connectionInstance;

    // Connect to the Database
    // before adding to the bplaced change credentials and than put tham back to default
    private function __construct($username = "root", $password = "", $host = "localhost", $dbname = "hotspot", $options = [])
    {
        $this->isConnected = TRUE;
        try {
            // Create the connection via PDO
            $this->database = new PDO("mysql:host=" . $host . ";dbname=" . $dbname . ";charset=utf8", $username, $password, $options);
            // Handle Connection errors by Exception
            $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // Use the fetch assoc for detail errors
            $this->database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     *
     * @return DB_Connection
     *
     * @since version
     */
    public static function getConnectionInstance()
    {
        if (self::$connectionInstance == null) {
            self::$connectionInstance = new DB_Connection();
        }
        return self::$connectionInstance;
    }
    /**
     *
     * Disconnect from Database
     * @since version
     */
    // Disconnect from Database
    public function disconnect()
    {
        $this->database = NULL;
        $this->isConnected = FALSE;
    }

    /**
     * @param $query
     * @param array $params
     *
     * @return mixed
     * @info get one row
     * calling this method u have to set the params for that row u r looking for.
     * params can not be empty
     * @since version 1.0
     * @throws Exception
     */
    public function getRow($query, $params = [])
    {
        try {
            $statement = $this->database->prepare($query);
            $statement->execute($params);

            return $statement->fetch();
        } catch (PDOException $ex) {
            throw new Exception($ex->getMessage());
        }
    }


    /**
     * @param $query
     * @param array $params
     *
     * @return multiple row, array
     *
     * @since version 1.0
     * @throws Exception
     */
    public function getRows($query, $params = [])
    {
        try {
            $statement = $this->database->prepare($query);
            $statement->execute($params);

            return $statement->fetchAll();
        } catch (PDOException $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * @param $query
     * @param array $params
     *
     * @return bool
     * @info insert row
     * @since version
     * @throws Exception
     */
    public function insertRow($query, $params = [])
    {
        try {
            $statement = $this->database->prepare($query);
            $statement->execute($params);

            return TRUE; // if query succeed return true
        } catch (PDOException $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * @param $query
     * @param array $params
     *
     * @info update row
     * @since version 1.0
     * @throws Exception
     */
    public function updateRow($query, $params = [])
    {
        try {
            $this->insertRow($query, $params);
        } catch (PDOException $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * @param $query
     * @param array $params
     *
     * @info delete row
     * @since version
     * @throws Exception
     */
    public function deleteRow($query, $params = [])
    {
        try {
            $this->insertRow($query, $params);
        } catch (PDOException $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * @info This method returns the connection which can be used for different procedural scripts
     * @return mysqli
     *
     * @since version 1.0
     */
    public function getConnection()
    {
        $connection = mysqli_connect("localhost", "root", "", "hotspot") or die(mysqli_connect_error());
        return $connection;
    }
    /* /. End */

    // declare the getter for the privat variables
    /**
     *
     * @return localhost
     *
     * @since version 1.0
     */
    public function getHost()
    {
        return $this->HOST;
    }

    /**
     *
     * @return Database User as String
     *
     * @since version 1.0
     */
    public function getUser()
    {
        return $this->USER;
    }

    /**
     *
     * @return loacalhost password as String (our password is empty)
     *
     * @since version 1.0
     */
    public function getPass()
    {
        return $this->PASS;
    }

    /**
     *
     * @return Database Name as String
     *
     * @since version 1.0
     */
    public function getDatabase()
    {
        return $this->DATABASE;
    }

}

?>