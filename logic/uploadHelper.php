<?php
/**
 * Created by PhpStorm.
 * User: GEO
 * Date: 18.12.2016
 * Time: 19:40
 */
include_once "DataUploader.php";

$uploader=new DataUploader();
$uploader->saveProband();
$uploader->saveTrackFile();
$uploader->savePicFile();
$uploader->saveHeartFreqFile();
$uploader->saveAppFile();
header("Location: ../pages/upload.php", true);
