<?php
/**
 * Created by PhpStorm.
 * User: GEO
 * Date: 29.12.2016
 * Time: 19:20
 */
include_once("../logic/HotSpotLogic.php");

function getProbLatLng($pnames = array())
{
    $trackData = [];
    $logicInst = new HotSpotLogic();
    $i = 0;
    $type = "";
    foreach ($pnames as $pname) {

        //$pName=str_replace($pname,"'","");
        $trackObj = $logicInst->getPTrackLatLngTime($pname);


        $trackData[$i] = array('proband' => $pname, 'track' => $trackObj);

        $i++;
    }

    $trackData = json_encode($trackData);

    echo $trackData;
}

function getProbLatLngArr($pnames = array())
{
    $trackData = [];
    $logicInst = new HotSpotLogic();
    $i = 0;
    $type = "";
    foreach ($pnames as $pname) {

        //$pName=str_replace($pname,"'","");
        $trackObj = $logicInst->getPTrackLatLngTime($pname);


        $trackData[$i] = array('proband' => $pname, 'track' => $trackObj);

        $i++;
    }

    //$trackData = json_encode($trackData);

    return $trackData;
}


if (isset($_POST['probNameTrack'])) {
    $pnames = $_POST['probNameTrack'];
    getProbLatLng($pnames);
}

if (isset($_POST['allProbsNammmmes'])) {
    $logicInst = new HotSpotLogic();
    $allProbs = $logicInst->getProbNames();
    echo json_encode($allProbs);
}

?>