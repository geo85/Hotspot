<?php

/**
 * Created by PhpStorm.
 * User: GEO
 * Date: 19.12.2016
 * Time: 22:32
 */
class KmlParse
{
    private $kmlTrackFile;
    private $parsedKml;

    /**
     * KmlParse constructor.
     * @param string $newProbTrackFile
     */
    public function __construct($newProbTrackFile)
    {
        $this->kmlTrackFile=$newProbTrackFile;

    }
    public function parseKml(){
        $this->parsedKml=simplexml_load_file($this->kmlTrackFile);
    }

    /**
     * @return string
     */
    public function getParsedKmlTrack()
    {

        $time = [];
        $lat = [];
        $lng = [];
        $parsedKml=simplexml_load_file($this->kmlTrackFile);


        $timeXML = $parsedKml->xpath('//gx:Track')[0]->when;
        $trackXML = $parsedKml->xpath('//gx:coord');
        foreach ($timeXML as $when) {

            $fullTime=explode("T",$when);
            $hourFullMilisec=$fullTime[1];
            $hourFull=explode(".",$hourFullMilisec);
            $hourMinSec=$hourFull[0];
            array_push($time,$hourMinSec);
            // echo "Time: ".$hourMinSec;
        }
        foreach ($trackXML as $coord) {
            $latXML=explode(" ",$coord);
            $lngXML=explode(" ",$coord);
            array_push($lng, $latXML[0]);
            array_push($lat, $lngXML[1]);
            // echo "lat: ".$lngXML[0]." ,lng: ".$lngXML[1];
        }
        $lngLatTime = [];
        for ($i = 0; $i < count($time); $i++) {
            array_push($lngLatTime, array("lng" => $lng[$i], "lat" => $lat[$i], "time" => $time[$i]));
        }
        return $lngLatTime;
    }

}