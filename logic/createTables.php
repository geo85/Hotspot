<?php

include_once("DB_Connection.php");
$db = DB_Connection::getConnectionInstance(); // declare the instance for DB

$tbl_prob_names = "CREATE TABLE IF NOT EXISTS pNames (
					unic_name VARCHAR(32) NOT NULL,
					PRIMARY KEY (unic_name)
				)";
$query = mysqli_query($db->getConnection(), $tbl_prob_names);
if ($query == TRUE) {
    echo "<h3> Probaden table created OK :) </h3>";
} else {
    echo "<h3> Probanden table not created :( </h3>";
}

///////////////////////////////////

$tbl_prob_track = "CREATE TABLE IF NOT EXISTS pTrack (
					pk_pT_id INT(11) NOT NULL AUTO_INCREMENT,
					latitude VARCHAR(32) NOT NULL,
					longtitude VARCHAR(32) NOT NULL,
					time VARCHAR(16) NOT NULL,
					pName VARCHAR(16) NOT NULL,
					PRIMARY KEY (pk_pT_id),
					FOREIGN KEY (pName) REFERENCES pNames(unic_name)
				)";
$query = mysqli_query($db->getConnection(), $tbl_prob_track);
if ($query == TRUE) {
    echo "<h3> Track table created OK :) </h3>";
} else {
    echo "<h3> Track table not created :( </h3>";
}

////////////////////////////////////

$tbl_prob_freq = "CREATE TABLE IF NOT EXISTS pFreq (
					pk_pF_id INT(11) NOT NULL AUTO_INCREMENT,
					freq VARCHAR(16) NOT NULL,
					time VARCHAR(16) NULL,
					pName VARCHAR(16) NOT NULL,
					PRIMARY KEY (pk_pF_id),
					FOREIGN KEY (pName) REFERENCES pNames(unic_name)
				)";
$query = mysqli_query($db->getConnection(), $tbl_prob_freq);
if ($query == TRUE) {
    echo "<h3> Freq table created OK :) </h3>";
} else {
    echo "<h3> Freq table not created :( </h3>";
}

////////////////////////////////////


$tbl_prob_app = "CREATE TABLE IF NOT EXISTS pApp (
					pk_pA_id INT(11) NOT NULL AUTO_INCREMENT,
					hotspot_name VARCHAR(256) NOT NULL,
					hotspot_bewertung VARCHAR(256) NOT NULL,
					latitude VARCHAR(32) NOT NULL,
					longtitude VARCHAR(32) NOT NULL,
					time VARCHAR(16) NOT NULL,
					bild_name VARCHAR(64),
					pName VARCHAR(16) NOT NULL,
					PRIMARY KEY (pk_pA_id),
					FOREIGN KEY (pName) REFERENCES pNames(unic_name)
				)";
$query = mysqli_query($db->getConnection(), $tbl_prob_app);
if ($query == TRUE) {
    echo "<h3> App table created OK :) </h3>";
} else {
    echo "<h3> App table not created :( </h3>";
}

////////////////////////////////////

$tbl_prob_hot_enter_Garden = "CREATE TABLE IF NOT EXISTS pEnter (
					pk_pE_id INT(11) NOT NULL AUTO_INCREMENT,
					zufrieden INT(2) NOT NULL,
					gluecklich INT(2) NOT NULL,
					ungluecklich INT(2) NOT NULL,
					unzufrieden INT(2) NOT NULL,
					wach INT(2) NOT NULL,
					munter INT(2) NOT NULL,
					schlapp INT(2) NOT NULL,
					muede INT(2) NOT NULL,
					gelassen INT(2) NOT NULL,
					entspannt INT(2) NOT NULL,
					angespannt INT(2) NOT NULL,
					nervoes INT(2) NOT NULL,
					pName VARCHAR(16) NOT NULL,
					PRIMARY KEY (pk_pE_id),
					FOREIGN KEY (pName) REFERENCES pNames(unic_name)
				)";
$query = mysqli_query($db->getConnection(), $tbl_prob_hot_enter_Garden);
if ($query == TRUE) {
    echo "<h3> Enter table created OK :) </h3>";
} else {
    echo "<h3> Enter table not created :( </h3>";
}

////////////////////////////////////

$tbl_prob_hot_left_Garden = "CREATE TABLE IF NOT EXISTS pLeft (
					pk_pL_id INT(11) NOT NULL AUTO_INCREMENT,
					zufrieden INT(2) NOT NULL,
					gluecklich INT(2) NOT NULL,
					ungluecklich INT(2) NOT NULL,
					unzufrieden INT(2) NOT NULL,
					wach INT(2) NOT NULL,
					munter INT(2) NOT NULL,
					schlapp INT(2) NOT NULL,
					muede INT(2) NOT NULL,
					gelassen INT(2) NOT NULL,
					entspannt INT(2) NOT NULL,
					angespannt INT(2) NOT NULL,
					nervoes INT(2) NOT NULL,
					pName VARCHAR(16) NOT NULL,
					PRIMARY KEY (pk_pL_id),
					FOREIGN KEY (pName) REFERENCES pNames(unic_name)
                  )";

$query = mysqli_query($db->getConnection(), $tbl_prob_hot_left_Garden);
if ($query == TRUE) {
    echo "<h3> Left table created OK :) </h3>";
} else {
    echo "<h3> Left table not created :( </h3>";
}

////////////////////////////////////

$tbl_prob_users = "CREATE TABLE IF NOT EXISTS hotspot_users(
                  user_id INT(11) NOT NULL AUTO_INCREMENT,
                  firstname VARCHAR(16) NOT NULL,
                  lastname VARCHAR(16) NOT NULL ,
                  email VARCHAR(16) NOT NULL,
                  password VARCHAR(16) NOT NULL ,
                  PRIMARY KEY (user_id)
                )";

$query = mysqli_query($db->getConnection(), $tbl_prob_users);
if ($query == TRUE) {
    echo "<h3> Users table created OK :) </h3>";
} else {
    echo "<h3> Users table not created :( </h3>";
}

?>