<?php

include_once "probFreqs.php";
include_once "probTracks.php";


function getAllProbTrackWeight()
{

    $logicInst = new HotSpotLogic();
    $probanden = $logicInst->showAllProbandenNames();
    $array = [];
    $i = 0;
    for ($j = 0; $j < sizeof($probanden); $j++) {
        $probTrack = getProbLatLngArr($probanden[$j]);
        for ($k = 0; $k < sizeof($probTrack); $k++) {

            $array[$j] = $probTrack[$k];

        }

    }
    $fre = json_encode($array);
    echo $fre;

}


function getAllProbFreqsWeight()
{
    $logicInst = new HotSpotLogic();
    $probanden = $logicInst->showAllProbandenNames();
    $array = [];
    $i = 0;
    for ($j = 0; $j < sizeof($probanden); $j++) {
        $probFreq = getProbFreqsArr($probanden[$j]);
        for ($k = 0; $k < sizeof($probFreq); $k++) {

            $array[$j] = $probFreq[$k];

        }

    }
    $fre = json_encode($array);
    echo $fre;


}

if (isset($_POST['googleHeatFreqWeight'])) {
    getAllProbFreqsWeight();
}

if (isset($_POST['googleHeatTrackWeight'])) {
    getAllProbTrackWeight();
}