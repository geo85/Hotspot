<?php
//header("Content-type: application/json");
include_once("probFreqs.php");
require_once("DB_Connection.php");

class HotSpotLogic{


    public function getAnzProbFromDatabase()
    {
        $db = DB_Connection::getConnectionInstance();
        $anz = $db->getRow("SELECT * FROM pnames", ["2"]);
		echo "<pre>";
		print_r($anz);
		echo "</pre>";
		//die();
	}

    public function getProbNames()
    {
        $db = DB_Connection::getConnectionInstance();
        $names = $db->getRows("SELECT pnames.unic_name FROM pnames");
        return ($names);
    }

    public function getProbNamesFromDatabase($prob)
    {
        $db = DB_Connection::getConnectionInstance();
		if ($prob == null){
            $names = $db->getRows("SELECT * FROM ptrack", [$prob]);
            echo "<pre>";
            $test = json_encode($names);
            echo $test;
            echo "</pre>";
            die();
		}
        $names = $db->getRows("SELECT * FROM ptrack WHERE pName = ?", [$prob]);
		echo "<pre>";
		print_r($names);
		echo "</pre>";
        die();
    }

    /**
     * @param $fname
     * @param $lname
     * @param $mail
     * @param $pass
     *
     *
     * @since version
     */
    public function insertUsers($fname, $lname, $mail, $pass)
    {
        $db = DB_Connection::getConnectionInstance();
        $insert = $db->insertRow("INSERT INTO hotspot_users(firstname, lastname, email, password) VALUES(?,?,?,?)", [$fname, $lname, $mail, $pass]);
	}


    /**
     * @param $probName
     *
     * @return mixed|string
     *
     * @since version 1.0
     */
    public function getPTrackTime($probName)
    {
        $db = DB_Connection::getConnectionInstance();
        if ($probName == null) {
            $time = $db->getRows("SELECT time FROM ptrack", [$probName]);
            return json_encode($time);
        }
        $time = $db->getRows("SELECT time FROM ptrack WHERE pName = ?", [$probName]);
        return json_encode($time);
    }

    /**
     * @param $probName
     *
     * @return mixed|string
     *
     * @since version 1.0
     */
    public function getPTrackLatLngTime($probName)
    {
        $db = DB_Connection::getConnectionInstance();
        if ($probName == null) {
            $latlngtime = $db->getRows("SELECT latitude AS lat, longtitude AS lng, time FROM ptrack", [$probName]);
            return ($latlngtime);
        }
        $latlngtime = $db->getRows("SELECT latitude AS lat, longtitude AS lng, time FROM ptrack WHERE pName = ?", [$probName]);
        return ($latlngtime);

	}

    /**
     * @param $probName
     *
     * @return mixed|string
     *
     * @since version 1.0
     */
    public function getPTrackLatLng($probName)
    {
        $db = DB_Connection::getConnectionInstance();
        if ($probName == null) {
            $latlng = $db->getRows("SELECT latitude AS lat, longtitude AS lng FROM ptrack", [$probName]);
            return ($latlng);
        }
        $latlng = $db->getRows("SELECT latitude AS lat, longtitude AS lng FROM ptrack WHERE pName = ?", [$probName]);
        return ($latlng);
    }

    public function getPFreq($probName)
    {
        $db = DB_Connection::getConnectionInstance();
        if ($probName == null) {
            $freq = $db->getRows("SELECT freq, time FROM pfreq");
            return ($freq);
        }
        $freq = $db->getRows("SELECT freq AS freq, time AS time FROM pfreq WHERE pName = ?", [$probName]);
        return ($freq);
    }

    public function concatPTrackPfreq($prob)
    {
        $db = DB_Connection::getConnectionInstance();
        if (mysqli_connect_errno()) {
            printf("DB Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        $qurery = "SELECT
                      ptrack.pName AS name,
                      ptrack.latitude AS lat,
                      ptrack.longtitude AS lng,
                      ptrack.time AS zeit,
                      pfreq.freq AS freq,
                      pfreq.time AS sekundentakt
                   FROM ptrack, pfreq
                   WHERE pk_pT_id = pk_pF_id
                   AND ptrack.pName = ?
                  ";
        $concat = array();
        $zeitArr = array();
        $counter = 0;
        if ($prob == null) {
            echo "This call of the Method<br>";
            echo($this->generateCallTrace());
        } else {
            try {
                $getData = $db->getRows($qurery, [$prob]);
                if (!$getData) {
                    die("Query" . $qurery . " failed! Couldn´t fetch data." . mysqli_connect_error());
                }
                foreach ($getData as $key => $val) {
                    foreach ($val as $item => $takt) {
                        switch ($item) {
                            case "name":
                                $zeitArr[$item] = $val[$item];
                                break;
                            case "zeit":
                                $zeitArr[$item] = $val[$item];
                                break;
                            case "freq":
                                $zeitArr[$item] = $val[$item];
                                break;
                            case "sekundentakt":
                                if ($takt == 0) {
                                    $counter++;
                                }
                                do {
                                    for ($i = 0; $i < 4; $i++) {
                                        $query = "UPDATE pfreq_kopie SET pfreq_kopie.time = ?, pfreq_kopie.freq=?
                                                  WHERE pfreq_kopie.pName = ?";
                                        $update = $db->updateRow($query, [$takt, $takt, "proband11"]);
                                    }
                                } while ($counter < 2);

                                $zeitArr[$item] = $val[$item];
                                break;
                        }
                    }
                    echo "<pre>";
                    print_r($zeitArr); //Still working on this method
                    echo "</pre>";
                }
            } catch (ErrorException $ex) {
                throw new ErrorException($ex->getMessage());
            }
        }
    }

    /**
     * This Method returns the Information off that calling Method which throws an Exception
     * Usage is for good for debugging
     * @return string
     *
     * @since version 1.0
     */
    private function generateCallTrace()
    {
        $e = new Exception();
        $trace = explode("\n", $e->getTraceAsString());
        // reverse array to make steps line up chronologically
        $trace = array_reverse($trace);
        array_shift($trace); // remove {main}
        array_pop($trace); // remove call to this method
        $length = count($trace);
        $result = array();

        for ($i = 0; $i < $length; $i++) {
            $result[] = ($i + 1) . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
        }

        return "\t" . implode("\n\t", $result);
    }

    public function getAppEnter()
    {
        $db = DB_Connection::getConnectionInstance();
        $enterData = $db->getRows("SELECT zufrieden,unzufrieden,gluecklich,ungluecklich,
  wach,munter,schlapp,muede,gelassen,entspannt,angespannt,nervoes from penter");
        return ($enterData);
    }

    public function getAppExit()
    {
        $db = DB_Connection::getConnectionInstance();
        $exitData = $db->getRows("select zufrieden,unzufrieden,gluecklich,ungluecklich,
  wach,munter,schlapp,muede,gelassen,entspannt,angespannt,nervoes from pleft");
        return ($exitData);
    }

    public function getAllAppEnter()
    {
        $db = DB_Connection::getConnectionInstance();
        $enterAllData = $db->getRows("select pName,zufrieden,unzufrieden,gluecklich,ungluecklich,
  wach,munter,schlapp,muede,gelassen,entspannt,angespannt,nervoes from penter");
        return ($enterAllData);

    }

    public function getAllAppExit()
    {
        $db = DB_Connection::getConnectionInstance();
        $exitAllData = $db->getRows("select pName,zufrieden,unzufrieden,gluecklich,ungluecklich,
                       wach,munter,schlapp,muede,gelassen,entspannt,angespannt,nervoes from pleft");
        return ($exitAllData);

    }

    /**
     *
     * @return multiple
     * @show Latitude, Longtitude und Hotspot-Bewertung von der Database
     *
     * @since version 1.0
     */
    public function getHotSpotsPosBewertung()
    {
        $db = DB_Connection::getConnectionInstance();

        $hotSpots = $db->getRows("SELECT pName,latitude, longtitude, hotspot_bewertung,
        bild_name,hotspot_name FROM papp");
        return ($hotSpots);
    }

    /**
     *
     * @return multiple
     * @show Hotspot-Name Latitude, Longtitude und Hotspot-Bewertung von der Database
     *
     * @since version 1.0
     */
    public function getGaugeDataForHeartFreqOs()
    {
        $db = DB_Connection::getConnectionInstance();

        $hotSpots = $db->getRows("SELECT hotspot_name, latitude, longtitude, hotspot_bewertung,bild_name FROM papp");
        return ($hotSpots);
    }

    public function showAllProbandenNames()
    {
        $db = DB_Connection::getConnectionInstance();
        $names = $db->getRows("SELECT pnames.unic_name FROM pnames");
        return ($names);
    }


    public function getAllProbFreqs()
    {
        $logicInst = new HotSpotLogic();
        $probanden = $logicInst->showAllProbandenNames();
        $array = [];
        $i = 0;
        for ($j = 0; $j < sizeof($probanden); $j++) {
            $probFreq = getProbFreqsArr($probanden[$j]);
            for ($k = 0; $k < sizeof($probFreq); $k++) {

                $array[$k] = $probFreq[$k];

            }

        }
        $fre = json_encode($array);
        echo $fre;

    }

    public function getAllHeartExitData()
    {


        $db = DB_Connection::getConnectionInstance();
        $endFreqs = $db->getRows("SELECT pName, freq FROM pfreq WHERE pk_pF_id IN 
        ( SELECT MAX(pk_pF_id) FROM pfreq GROUP BY pName)");
        return ($endFreqs);


    }

    public function getAllHeartEnterData()
    {
        $db = DB_Connection::getConnectionInstance();
        $startFreqs = $db->getRows("SELECT  DISTINCT (pName), freq from pfreq GROUP BY pName");
        return ($startFreqs);
    }




    /*
     * Kill the Database-connection
     */
    public function killConnection()
    {
        die();
	}
}

?>
