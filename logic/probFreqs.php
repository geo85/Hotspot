<?php

include_once("../logic/HotSpotLogic.php");

function getProbFreqs($pnames = array())
{
    $trackData = [];
    $logicInst = new HotSpotLogic();
    $i = 0;
    $type = "";
    foreach ($pnames as $pname) {

        //$pName=str_replace($pname,"'","");
        $freqObj = $logicInst->getPFreq($pname);


        $freqData[$i] = array('proband' => $pname, 'freq' => $freqObj);

        $i++;
    }

    $freqData = json_encode($freqData);

    echo $freqData;
}

function getProbFreqsArr($pnames = array())
{
    $trackData = [];
    $logicInst = new HotSpotLogic();
    $i = 0;
    $type = "";
    foreach ($pnames as $pname) {

        //$pName=str_replace($pname,"'","");
        $freqObj = $logicInst->getPFreq($pname);


        $freqData[$i] = array('proband' => $pname, 'freq' => $freqObj);

        $i++;
    }

    // $freqData = json_decode($freqData);

    return $freqData;
}


if (isset($_POST['probNameFreq'])) {
    $pnames = $_POST['probNameFreq'];
    getProbFreqs($pnames);
}


?>