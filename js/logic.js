var output = null;// global for accesing from anyfunction

// return the path of all jsons
function getProbandenFiles(container) {
    var request;
    if (window.XMLHttpRequest){
        request = new XMLHttpRequest();
    }else{
        request = new ActiveXObject('Microsoft.XMLHTTP');
    }
    request.open('GET', '../json/probanden.json');
    request.onreadystatechange = function () {
        if ((request.readyState === 4) && (request.status === 200)){
            var items = JSON.parse(request.responseText);
            output = '<ul>';
            for (var key in items){
                output += '<li>' + items[key].probandAppFile + '</li>';
            }
            output += '</ul>';
            document.getElementById(container).innerHTML = output;
        }
    };
    return request.send();
}



// returnd the probandenHeart file path
function getProbandenHeartFiles(container){
    var request;
    if (window.XMLHttpRequest){
        request = new XMLHttpRequest();
    }else{
        request = new ActiveXObject('Microsoft.XMLHTTP');
    }
    request.open('GET', '../json/probanden.json');
    request.onreadystatechange = function () {
        if ((request.readyState === 4) && (request.status === 200)){
            var items = JSON.parse(request.responseText);
            var output = '<ul>';
            for (var key in items){
                output += '<li>' + items[key].probandHeartFile + '</li>';
            }
            output += '</ul>';
            document.getElementById(container).innerHTML = output;
        }
    };
    return request.send();
}

// returnd the probandenHeart file path
function getProbandenTrackFiles(container){
    var request;
    if (window.XMLHttpRequest){
        request = new XMLHttpRequest();
    }else{
        request = new ActiveXObject('Microsoft.XMLHTTP');
    }
    request.open('GET', '../json/probanden.json');
    request.onreadystatechange = function () {
        if ((request.readyState === 4) && (request.status === 200)){
            var items = JSON.parse(request.responseText);
            var output = '<ul>';
            for (var key in items){
                output += '<li>' + items[key].probandTrackFile + '</li>';
            }
            output += '</ul>';
            document.getElementById(container).innerHTML = output;
        }
    };
    return request.send();
}



