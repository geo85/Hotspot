/**
 * Created by GEO on 11.11.2016.
 */

document.addEventListener('DOMContentLoaded', function () {
    if (document.querySelectorAll('#map-container').length > 0)
    {

        if (document.querySelector('html').lang)
            lang = document.querySelector('html').lang;
        else
            lang = 'en';
        var js_file = document.createElement('script');
        js_file.type = 'text/javascript';
        js_file.src = 'https://maps.googleapis.com/maps/api/js?callback=initMap&sensor=false&key=AIzaSyDaQOhP0x2wGnkg8k7yxx9ynjRtc3Wyjvc';
        document.getElementsByTagName('head')[0].appendChild(js_file);

    }
});
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map-container'), {

        center: {
            lat: 48.781547,
            lng: 9.1745084
        },
        zoom: 15
    });
}