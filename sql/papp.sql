-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 27. Dez 2016 um 19:32
-- Server-Version: 10.1.16-MariaDB
-- PHP-Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `hotspot`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `papp`
--

CREATE TABLE `papp` (
  `pk_pA_id`          INT(11)     NOT NULL,
  `hotspot_name`      VARCHAR(16) NOT NULL,
  `hotspot_bewertung` VARCHAR(16) NOT NULL,
  `latitude`          VARCHAR(16) NOT NULL,
  `longtitude`        VARCHAR(16) NOT NULL,
  `time`              VARCHAR(16) NOT NULL,
  `bild_name`         VARCHAR(20) DEFAULT NULL,
  `pName`             VARCHAR(16) NOT NULL
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Daten für Tabelle `papp`
--

INSERT INTO `papp` (`pk_pA_id`, `hotspot_name`, `hotspot_bewertung`, `latitude`, `longtitude`, `time`, `bild_name`, `pName`)
VALUES
  (1, 'Grün, Kunst, Lie', '6', '48.7815781', '9.1734818', '15:27:47', '1465997285618.jpg', 'proband11'),
  (2, 'Entspannen und s', '8', '48.782123', '9.1745203', '16:14:46', '1466000122548.jpg', 'proband12'),
  (3, 'Schöne Blumen un', '7', '48.7808573', '9.1738508', '16:14:46', '1466000358426.jpg', 'proband12'),
  (4, 'Keine Schöne Aus', '3', '48.7809402', '9.1731454', '16:21:19', '1466000509359.jpg', 'proband12'),
  (5, 'Sitzgelegenheite', '5', '48.781488', '9.1725134', '16:22:46', '1466000630629.jpg', 'proband12'),
  (6, 'Alles grau, Müll', '1', '48.7817791', '9.1725356', '12:21:07', '1466158902579.jpg', 'proband13'),
  (7, 'Holzsitze modrig', '2', '48.781582', '9.1736235', '13:16:36', '1466767029513.jpg', 'proband16'),
  (8, 'Statuen', '8', '48.7814233', '9.1737586', '13:16:36', '1466767103426.jpg', 'proband16'),
  (9, 'Müllcontainer', '0', '48.7820001', '9.1743807', '12:51:58', '1471431165761.jpg', 'proband18'),
  (10, 'Grüne Liegefläch', '6', '48.780969', '9.1739086', '12:51:58', '1471431317406.jpg', 'proband18'),
  (11, 'Weg durch Blumen', '8', '48.7808856', '9.1737922', '12:56:09', '1471431411374.jpg', 'proband18'),
  (12, 'Kahle Fläche, Be', '0', '48.781592', '9.172785', '12:58:47', '1471431559817.jpg', 'proband18');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `papp`
--
ALTER TABLE `papp`
  ADD PRIMARY KEY (`pk_pA_id`),
  ADD KEY `pName` (`pName`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `papp`
--
ALTER TABLE `papp`
  MODIFY `pk_pA_id` INT(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 13;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `papp`
--
ALTER TABLE `papp`
  ADD CONSTRAINT `papp_ibfk_1` FOREIGN KEY (`pName`) REFERENCES `pnames` (`unic_name`);

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
