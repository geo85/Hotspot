﻿﻿
<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=utf-8">
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Hotspot hunter v1.0</a>
        <style>
            #one {
                width: 300px;
                position: relative;
                left: 250px;
            }
        </style>
        <span>
            <img src="../images/heartbeat.gif" id="one">
        <span>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-comment fa-fw"></i> Neuer Kommentar
                            <span class="pull-right text-muted small">vor 4 Minuten</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-twitter fa-fw"></i> 3 neue Followers
                            <span class="pull-right text-muted small">vor 12 Minuten</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-envelope fa-fw"></i> Gesendete Nachrichten
                            <span class="pull-right text-muted small">vor 4 Minuten</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-tasks fa-fw"></i> Neuer Track
                            <span class="pull-right text-muted small">vor 4 Minuten</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                            <span class="pull-right text-muted small">vor 4 Minuten</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>Alle Notifications ansehen</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-alerts -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <li>
                    <a href="#">
                        <div>
                            <strong>Ermal</strong>
                            <span class="pull-right text-muted">
                                <em>Gestern</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <strong>George</strong>
                            <span class="pull-right text-muted">
                                <em>Gestern</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <strong>Ali</strong>
                            <span class="pull-right text-muted">
                                <em>Gestern</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>Lies alles</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-messages -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-tasks">
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Track 1</strong>
                                <span class="pull-right text-muted">80% Zufrieden</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                    <span class="sr-only">80% Zufrieden (success)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Track 2</strong>
                                <span class="pull-right text-muted">40% Zufrieden</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Zufrieden</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Track 3</strong>
                                <span class="pull-right text-muted">30% Befriedigend</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="30"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                                    <span class="sr-only">30% Befriedigend (warning)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Track 4</strong>
                                <span class="pull-right text-muted">70% Unzufrieden</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                    <span class="sr-only">70% Unzufrieden (danger)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>Alle Tracks sehen</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-tasks -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#" data-toggle="modal" data-target="#login-modal">
                        <i class="fa fa-sign-out fa-fw"></i> Einlogen
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Suchen...">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="#arrow-down"><i class="fa fa-globe fa-fw"></i> Heatmap Visualisierung<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="openstreetmap.php"><i class="fa fa-bullseye fa-fw"></i> OpenstreetMap</a>
                        </li>
                        <li>
                            <a href="googlemap.php"><i class="fa fa-google fa-fw"></i> Google Map</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#arrow-down"><i class="fa fa-bar-chart fa-fw"></i> Charts<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="appRating.php"><i class="fa fa-align-left  fa-fw" ></i> App-Rating</a>
                        </li>
                        <li>
                            <a href="heartFreq.php"><i class="fa fa-heartbeat fa-fw" ></i> Heart frequency</a>
                        </li>
                        <li>
                            <a href="hotSpotAnalyzer.php"><i class="fa fa-map-marker fa-fw"></i> Hotspoter-Analyzer
                            </a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li id="dreiundzwanziguhrfuenfundfuenfzig">
                    <a href="#arrow-down" id="hashTagPage"><i class="fa fa-hashtag fa-fw"></i> Hashtags<span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level" id="scrollTags">
                        <?php
                        include_once "../logic/DB_Connection.php";
                        $db = DB_Connection::getConnectionInstance();
                        $names = "SELECT * FROM papp ";
                        $result = mysqli_query($db->getConnection(), $names);
                        if ($result->num_rows > 0) {
                            // output data of each row

                            while ($row = $result->fetch_assoc()) {
                                $bewertung = $row["hotspot_bewertung"];
                                $pic = $row["bild_name"];
                                $lng = $row["longtitude"];
                                $lat = $row["latitude"];
                                $nonenc = $row["hotspot_name"];
                                $hashname = mb_convert_encoding($nonenc, "UTF-8", "ISO-8859-1");
                                echo '<li><a type="submit"
                                onclick="showMarker(\'' . $lat . '\',\'' . $lng . '\',\'' . $pic . '\',\'' . $bewertung . '\'
                                ,\'' . $hashname . '\');">' .
                                    $hashname . '</a></li>';
                            }
                        } else {
                            echo "Keine Hashtags";
                        }
                        $db->disconnect();
                        ?>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="getLatLong.php"><i class="fa fa-map-marker fa-fw"></i> Latitude & Longitude Finder</a>
                </li>
                <li>
                    <a href="upload.php"><i class="fa fa-edit fa-fw"></i> Upload</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

<style>
    /* scrollebla menu */
    #scrollTags {
        overflow-y: scroll;
        height: 200px;;
        top: 0;
        bottom: 0;
    }
</style>

<!-- BEGIN # MODAL LOGIN -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="../images/1.jpg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>

            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <!-- Begin # Login Form -->
                <form id="login-form">
                    <div class="modal-body">
                        <div id="div-login-msg">
                            <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-login-msg">Type your username and password.</span>
                        </div>
                        <br>
                        <input id="login_username" class="form-control" type="text"
                               placeholder="Username (type ERROR for error effect)" required>
                        <br>
                        <input id="login_password" class="form-control" type="password" placeholder="Password" required>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                        </div>
                        <div>
                            <button id="login_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                            <button id="login_register_btn" type="button" class="btn btn-link">Register</button>
                        </div>
                    </div>
                </form>
                <!-- End # Login Form -->

                <!-- Begin | Lost Password Form -->
                <form id="lost-form" style="display:none;" action="">
                    <div class="modal-body">
                        <div id="div-lost-msg">
                            <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-lost-msg">Type your e-mail.</span>
                        </div>
                        <br>
                        <input id="lost_email" class="form-control" type="text"
                               placeholder="E-Mail (type ERROR for error effect)" required>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
                        </div>
                        <div>
                            <button id="lost_login_btn" type="button" class="btn btn-link">Log In</button>
                            <button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
                        </div>
                    </div>
                </form>
                <!-- End | Lost Password Form -->

                <!-- Begin | Register Form -->
                <form id="register-form" style="display:none;">
                    <div class="modal-body">
                        <div id="div-register-msg">
                            <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                            <span id="text-register-msg">Register an account.</span>
                        </div>
                        <br>
                        <input id="register_username" class="form-control" type="text"
                               placeholder="Username (type ERROR for error effect)" required>
                        <br>
                        <input id="register_email" class="form-control" type="text" placeholder="E-Mail" required>
                        <br>
                        <input id="register_password" class="form-control" type="password" placeholder="Password"
                               required>
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                        </div>
                        <div>
                            <button id="register_login_btn" type="button" class="btn btn-link">Log In</button>
                            <button id="register_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                        </div>
                    </div>
                </form>
                <!-- End | Register Form -->
            </div>
            <!-- End # DIV Form -->
        </div>
    </div>
</div>
<!-- END # MODAL LOGIN -->

<script>
    $(function () {

        var $formLogin = $('#login-form');
        var $formLost = $('#lost-form');
        var $formRegister = $('#register-form');
        var $divForms = $('#div-forms');
        var $modalAnimateTime = 300;
        var $msgAnimateTime = 150;
        var $msgShowTime = 2000;

        $("form").submit(function () {
            switch (this.id) {
                case "login-form":
                    var $lg_username = $('#login_username').val();
                    var $lg_password = $('#login_password').val();
                    if ($lg_username == "ERROR") {
                        msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");
                    } else {
                        msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Login OK");
                    }
                    return false;
                    break;
                case "lost-form":
                    var $ls_email = $('#lost_email').val();
                    if ($ls_email == "ERROR") {
                        msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
                    } else {
                        msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
                    }
                    return false;
                    break;
                case "register-form":
                    var $rg_username = $('#register_username').val();
                    var $rg_email = $('#register_email').val();
                    var $rg_password = $('#register_password').val();
                    if ($rg_username == "ERROR") {
                        msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", "Register error");
                    } else {
                        msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "success", "glyphicon-ok", "Register OK");
                    }
                    return false;
                    break;
                default:
                    return false;
            }
        });

        $('#login_register_btn').click(function () {
            modalAnimate($formLogin, $formRegister)
        });
        $('#register_login_btn').click(function () {
            modalAnimate($formRegister, $formLogin);
        });
        $('#login_lost_btn').click(function () {
            modalAnimate($formLogin, $formLost);
        });
        $('#lost_login_btn').click(function () {
            modalAnimate($formLost, $formLogin);
        });
        $('#lost_register_btn').click(function () {
            modalAnimate($formLost, $formRegister);
        });
        $('#register_lost_btn').click(function () {
            modalAnimate($formRegister, $formLost);
        });

        function modalAnimate($oldForm, $newForm) {
            var $oldH = $oldForm.height();
            var $newH = $newForm.height();
            $divForms.css("height", $oldH);
            $oldForm.fadeToggle($modalAnimateTime, function () {
                $divForms.animate({height: $newH}, $modalAnimateTime, function () {
                    $newForm.fadeToggle($modalAnimateTime);
                });
            });
        }

        function msgFade($msgId, $msgText) {
            $msgId.fadeOut($msgAnimateTime, function () {
                $(this).text($msgText).fadeIn($msgAnimateTime);
            });
        }

        function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
            var $msgOld = $divTag.text();
            msgFade($textTag, $msgText);
            $divTag.addClass($divClass);
            $iconTag.removeClass("glyphicon-chevron-right");
            $iconTag.addClass($iconClass + " " + $divClass);
            setTimeout(function () {
                msgFade($textTag, $msgOld);
                $divTag.removeClass($divClass);
                $iconTag.addClass("glyphicon-chevron-right");
                $iconTag.removeClass($iconClass + " " + $divClass);
            }, $msgShowTime);
        }
    });
</script>

<script>
    $(document).ready(function () {
        var url = window.location.href;
        if (url.includes("hashTagPage.php")) {
            document.getElementById("hashTagPage").removeAttribute("id");
            document.getElementById("dreiundzwanziguhrfuenfundfuenfzig").setAttribute("class", "active");
        } else {
            document.getElementById("hashTagPage").onclick = function () {
                location.replace("hashTagPage.php");
                document.getElementById("dreiundzwanziguhrfuenfundfuenfzig").setAttribute("class", "active");
            }
        }
    });
</script>

