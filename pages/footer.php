﻿<footer class="footer">
    <div class="container">
        <p class="text-muted"><strong><span id="dynamicDate"></span></strong> Hft-Stuttgart! Projekt Hotspot (Gruppe 6, 2016/17) Fach: Geo Visualisierung</p>
    </div>
</footer>
<script>
    // define the months
    var months = [
                    'Januar', 'Februar', 'März',
                    'April', 'Mai', 'Juni',
                    'Juli', 'August', 'September',
                    'Oktober', 'November', 'Dezember'
                 ];

    // define date
    var date = new Date();
    // store date
    date.setTime(date.getTime() + (3600*24));
    // get id of span
    document.getElementById("dynamicDate").innerHTML = date.getDate() +" "+ months[date.getMonth()] +" "+ date.getFullYear().toString();
</script>
