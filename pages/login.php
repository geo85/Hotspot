﻿<?php

class User
{

    private $user_id;
    private $firstname;
    private $lastname;
    private $email;
    private $password;

    /**
     * User constructor.
     * @param $id
     * @param $fname
     * @param $lname
     * @param $mail
     * @param $pass
     */
    public function __construct($id, $fname, $lname, $mail, $pass)
    {
        $this->user_id = $id;
        $this->firstname = $fname;
        $this->lastname = $lname;
        $this->email = $mail;#
        $this->password = $pass;
    }

    /**
     * @param $name
     *
     * @return set name for user
     *
     * @since version
     */
    public function _setFirstName($name)
    {
        return $this->firstname = $name;
    }


}

?>