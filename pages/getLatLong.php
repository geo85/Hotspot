﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
        #map-container { height: 350px }
    </style>
    <title>Hotspot hunter v1.0 Get latitude & longtitude</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    -->
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- add library script first -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaQOhP0x2wGnkg8k7yxx9ynjRtc3Wyjvc&libraries=places"></script>
<!-- add logic to autocomplete the address -->
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', initilize);
    function initilize() {
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById("textAutoComplete"));

        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            var location = "Adresse ist: " + place.formatted_address + "<br/>";
            location += "Latitude: " + place.geometry.location.lat()+ "<br/>";
            location += "Longitude: "  + place.geometry.location.lng();
            document.getElementById('lblresult').innerHTML = location
        });

    }
</script>
<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php") ?>
    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="headi">Latitude und Longtitude Finder</small></h3>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row header tittle-->

        <!-- row welcome text-->
        <div class="row">
            <!-- welcome text col-lg-12-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Finde die Latitude und Longtitude einer beliebigen Position
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <span>Location:</span><input type="text" class="form-control" id="textAutoComplete"
                                                     placeholder="Bitte Ort eingeben..."/>
                        <br>
                        <label id="lblresult"></label>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.welcome tet col-lg-12 -->
        </div>
        <!-- /.row welcome text-->

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>



