﻿<?php
require_once("../logic/DB_Connection.php");
include_once("../logic/HotSpotLogic.php");
/**
 * This method connect to the @show_app_track View
 * and get all the Data [LAT LONG]
 * @since version 1.0
 */
function showLatLngTrack()
{
    $db = DB_Connection::getConnectionInstance();
    $tracks = "SELECT * FROM ptrack";
    $result = mysqli_query($db->getConnection(), $tracks);
    $osData = [];
    $i = 0;

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $lng = $row["longtitude"];
            $lat = $row["latitude"];
            $osData[$i] = $lat . ", " . $lng;
            $i++;
        }
        $itemsInArray = 0;
        $countItems = count($osData);
        $i = 0;
        foreach ($osData as $item) {
            echo '{location: new google.maps.LatLng(' . $item . '), weight:' . mt_rand(60, 190) . '},';
            $i++;
            // see if the element of the array is the last index
            // if so than remove the ',' to have better array in javascript call
            if (++$itemsInArray === $countItems) {
                echo '{location: new google.maps.LatLng(' . $item . '), weight:' . mt_rand(60, 190) . '}';
                $i++;
            }
        }
    } else {
        echo "Keine Tracks gefunden";
    }
    //$db->disconnect();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
        #map-container {
            height: 350px
        }

        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto', 'sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #floating-panel {
            background-color: #fff;
            border: 0.5px solid #999;
            left: 25%;
            padding: 5px;
            position: absolute;
            top: 56px;
            z-index: 5;
        }
    </style>
    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    -->
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="../js/moment.js"></script>
</head>

<body>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php"); ?>

    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">Darstellung mit der Google-Map API</h3>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row header tittle-->

        <!-- row welcome text-->
        <div class="row">
            <!-- welcome text col-lg-12-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Heatmap zu den Probanden
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="floating-panel" style="border-radius: 7px;">
                            <button onclick="toggleHeatmap()">Toggle Heatmap</button>
                            <button onclick="changeGradient()">Change gradient</button>
                            <button onclick="changeRadius()">Change radius</button>
                            <button onclick="changeOpacity()">Change opacity</button>
                        </div>
                        <div id="map-container"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.welcome tet col-lg-12 -->
        </div>
        <!-- /.row welcome text-->

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->


</div>
<!-- /#wrapper -->

<!-- jQuery -->

<script src="../vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<script type="text/javascript">

    ajaxCallPhpHeart();
    ajaxCallPhpTrack();
    var weightDataObjects = getHeartDataWeight();

    //console.log(weightDataObjects);
    var trackDataObjects = getTracktDataWeight();

    //console.log(trackDataObjects);

    function ajaxCallPhpHeart() {
        $.ajax({
            url: '../logic/googleFreqWeight.php',
            type: 'POST',
            data: {googleHeatFreqWeight: 'googleHeatFreqWeight'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                setHeartDataWeight(data);
            }
        });
    }

    var heartDataWeight = [];
    function setHeartDataWeight(hData) {
        heartDataWeight = hData;
    }

    function getHeartDataWeight() {
        return heartDataWeight;
    }

    function ajaxCallPhpTrack() {
        $.ajax({
            url: '../logic/googleFreqWeight.php',
            type: 'POST',
            data: {googleHeatTrackWeight: 'googleHeatTrackWeight'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                setTracktDataWeight(data);
            }
        });
    }

    var trackDataWeight = [];
    function setTracktDataWeight(hData) {
        trackDataWeight = hData;
    }

    function getTracktDataWeight() {
        return trackDataWeight;
    }

    function getGoogleMapsPreparedData() {
        var name = [];
        var lat = [];
        var lng = [];
        var weight = [];
        var startTime = [];
        var allTracks = [];
        for (var i = 0; i < trackDataObjects.length; i++) {
            name[i] = trackDataObjects[i].proband;
            //console.log(name);
            for (var j = 0; j < trackDataObjects[i].track.length; j++) {
                lat[j] = trackDataObjects[i].track[j].lat;
                lng[j] = trackDataObjects[i].track[j].lng;
                startTime[j] = trackDataObjects[i].track[0].time;
                //console.log(name[i], lat[j], lng[j], startTime[j]);
                weight[j] = getLatLngWeightFromProband(name[i], lat[j], lng[j], startTime[j]);
                //console.log(weight[j]);
                allTracks.push({lat: parseFloat(lat[j]), lng: parseFloat(lng[j]), weight: weight[j]});
            }

            //allTracks.push(name, lat, lng, startTime, weight);
            //console.log(lat,lng,startTime);
            //console.log(allTracks);
            //console.log(weight);
        }
        return allTracks;
    }
    var prepData = getGoogleMapsPreparedData();
    //console.log(prepData);


    function getLatLngWeightFromProband(probandName, lat, lng, startTime) {
        var freqByIndex;
        var probObj;
        for (var i = 0; i < trackDataObjects.length; i++) {
            if (probandName === trackDataObjects[i].proband) {
                probObj = trackDataObjects[i].track;
                //console.log(probObj);
            }
        }
        var latStringFromEvent = lat;
        var lngStringFromEvent = lng;
        latStringFromEvent = latStringFromEvent.toString().slice(0, 13);
        var latFloatFromEvent = parseFloat(latStringFromEvent);
        lngStringFromEvent = lngStringFromEvent.toString().slice(0, 13);
        var lngFloatFromEvent = parseFloat(lngStringFromEvent);
        //console.log(probObj);

        var objlat = [];
        var objlng = [];
        var objtime = [];
        for (var k = 0; k < probObj.length; k++) {
            objlat[k] = probObj[k].lat;
            objlng[k] = probObj[k].lng;
            objtime[k] = probObj[k].time;
            //console.log(objlat[k], objlng[k], objtime[k]);
            //console.log("--------------------------------OBJLAT--------------------------");
            //console.log(objlat, objlng);
            //console.log("--------------------------------OBJLAT--------------------------");


            var latStringFromObj = objlat[k];
            var lngStringFromObj = objlng[k];

            //console.log(latStringFromObj,lngStringFromObj);

            latStringFromObj = latStringFromObj.slice(0, 13);
            var latFloatFromObj = parseFloat(latStringFromObj);
            lngStringFromObj = lngStringFromObj.slice(0, 13);
            var lngFloatFromObj = parseFloat(lngStringFromObj);
            //console.log(latFloatFromObj, latFloatFromEvent, lngFloatFromObj, lngFloatFromEvent);
            if (latFloatFromObj == latFloatFromEvent && lngFloatFromObj == lngFloatFromEvent) {
                // console.log("EQUAL");
                //console.log("This is Object Time", objtime);
                var start = moment("2017-01-14 " + startTime);
                //console.log("TrackTime: ", objtime[k]);
                //console.log("StartTime: ", startTime);
                var now = moment("2017-01-14 " + objtime[k]);
                //console.log("This is Object Now", now);
                var differenceInMs = now.diff(start, 's');
                // console.log(differenceInMs);
                var diffDividefour = parseInt((differenceInMs / 4));
                //console.log("This is Object Divedefour", diffDividefour);
                freqByIndex = getFreqByIndex(probandName, diffDividefour);
                //console.log(freqByIndex);
                return freqByIndex;

            }
        }

    }

    function getFreqByIndex(probandName, arrayIndex) {
        //console.log("getFreqByIndex --> ",arrayIndex);
        var freq;
        var prob = [];
        for (var i = 0; i < weightDataObjects.length; i++) {
            if (weightDataObjects[i].proband === probandName) {
                prob = weightDataObjects[i].freq;
                //console.log("DAS IST MEIN PROB ---> ",prob);
            }
        }
        if (prob[arrayIndex]) {
            //console.log("DAS IS PROB ARRAYINDE",prob[arrayIndex]);
            freq = parseInt(prob[arrayIndex].freq);
        } else {
            freq = 77;
        }
        //console.log("Freq---> ",freq);
        return freq;
    }

    //console.log(getLatLngWeightFromProband("proband11", 48.78163146972656, 9.177519798278809, "15:20:12"));

    //{location: new google.maps.LatLng(48.78163146972656, 9.177519798278809), weight:169}


    //-------------------------GOOGLE INIT DOWN-----------------------------------------------------------

    function initMapHeatMap() {
        map = new google.maps.Map(document.getElementById('map-container'), {
            zoom: 16,
            center: {lat: 48.7815, lng: 9.174},
            mapTypeId: google.maps.MapTypeId.SATELLITE
        });
        createMarkers();
        var mapPointsWeight = [];
        var latLngs = [];
        for (var z = 0; z < prepData.length; z++) {
            latLngs[z] = new google.maps.LatLng(prepData[z].lat, prepData[z].lng);

        }
        for (var y = 0; y < prepData.length; y++) {
            mapPointsWeight[y] = {location: latLngs[y], weight: prepData[y].weight};
        }

        //console.log(mapPointsWeight);
        heatmap = new google.maps.visualization.HeatmapLayer({
            data: mapPointsWeight,
            map: map
        });
    }

    function toggleHeatmap() {

        heatmap.setMap(heatmap.getMap() ? null : map);
    }

    function changeGradient() {
        var gradient = [
            'rgba(0, 255, 255, 0)',
            'rgba(0, 255, 255, 1)',
            'rgba(0, 191, 255, 1)',
            'rgba(0, 127, 255, 1)',
            'rgba(0, 63, 255, 1)',
            'rgba(0, 0, 255, 1)',
            'rgba(0, 0, 223, 1)',
            'rgba(0, 0, 191, 1)',
            'rgba(0, 0, 159, 1)',
            'rgba(0, 0, 127, 1)',
            'rgba(63, 0, 91, 1)',
            'rgba(127, 0, 63, 1)',
            'rgba(191, 0, 31, 1)',
            'rgba(255, 0, 0, 1)'
        ];
        heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
    }

    function changeRadius() {
        heatmap.set('radius', heatmap.get('radius') ? null : 20);
    }

    function changeOpacity() {
        heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
    }

    // Heatmap data: 500 Points
    function getPoints() {
        return [
            <?php showLatLngTrack(); ?>
        ];
    }

    var map, heatmap;


    function createMarkers() {

        $.ajax({
            url: '../logic/hotSpotter.php',
            data: '',
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                var hotspots = data;
                var markers = [];
                var positions = [];
                var infowindows = [];
                var contentString = [];
                console.log(hotspots);
                for (var i = 0; i < hotspots.length; i++) {
                    var marker = markers[i];
                    var infowindow = infowindows[i];
                    var icon = {
                        url: "../images/marker_" + hotspots[i].hotspot_bewertung + ".png", // url
                        scaledSize: new google.maps.Size(20, 20), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    };
                    var picSrc = "../probandenData/images/" + hotspots[i].bild_name;
                    contentString[i] = "Proband: <b>" + hotspots[i].pName + '</b><br>Hashtag: <b>' + hotspots[i].hotspot_name
                        + '</b><br><IMG BORDER="0" ALIGN="center" height="70 px" width="90 px" ' +
                        'SRC=' + picSrc + '> ';
                    infowindows[i] = new google.maps.InfoWindow({
                        maxWidth: 100
                    });
                    positions[i] = new google.maps.LatLng(hotspots[i].latitude
                        , hotspots[i].longtitude);
                    markers[i] = new google.maps.Marker({
                        position: positions[i],
                        map: map,
                        icon: icon,
                        //animation: google.maps.Animation.BOUNCE
                    });
                    bindInfoWindow(markers[i], map, infowindows[i], contentString[i]);
                }
                //setMarkers(markers);
            }

        });

    }

    function bindInfoWindow(marker, map, infowindow, description) {
        marker.addListener('click', function () {
            infowindow.setContent(description);
            infowindow.open(map, this);
        });
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaQOhP0x2wGnkg8k7yxx9ynjRtc3Wyjvc&signed_in=true&libraries=visualization&callback=initMapHeatMap">
</script>

</body>

</html>



