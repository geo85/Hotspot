﻿<?php require_once("../logic/DB_Connection.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hotspot hunter v1.0</title>


    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        #prob-names {
            z-index: 10;
            height: 100%;
            max-height: 501px;
            overflow-y: scroll;
        }

        #list-view {
        }

        #list-view h7 {
            font: 400 40px/1.5 Helvetica, Verdana, sans-serif;
            margin: 0;
            padding: 0;
        }

        #list-view ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #list-view ol {
            margin: 8px;
            padding: 2px;
            list-style-type: none;
        }

        #list-view li {
            font: 200 15px/1.5 Helvetica, Verdana, sans-serif;
            border-bottom: 1px solid #ccc;
        }

        #list-view li:last-child {
            border: none;
        }

        #list-view li a {
            text-decoration: none;
            color: #000;

            -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
            -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
            -o-transition: font-size 0.3s ease, background-color 0.3s ease;
            -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
            transition: font-size 0.3s ease, background-color 0.3s ease;
            display: block;
            width: auto;
        }

        #list-view li a:hover {
            font-size: 30px;
            background: #f6f6f6;
        }

    </style>

</head>

<body>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php"); ?>

    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Upload</h1>
                <form method="post" action="../logic/uploadHelper.php" enctype="multipart/form-data"
                      onsubmit="ShowUploading()">
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading ">
                                <b>Probanden in Hotspot hochladen</b>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Name des Probanden</label>
                                                <input placeholder="hier den Namen des Probanden eingeben"
                                                       class="form-control" type="text" name="uploadProbName"
                                                       id="uploadProbName" required>
                                            </div>
                                            <div class="alert alert-danger" role="alert">
                                                !!! Probanden Namen sind eindeutig zu w&aumlhlen !!!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Herz-Freq Datei</label>
                                                <input type="file" name="uploadHeartFreq"
                                                       id="uploadHeartFreq" required>
                                            </div>
                                            <div class="alert alert-danger" role="alert">
                                                bitte eine g&uumlltige <b>.csv</b> Datei unter
                                                01_Messdaten\[Probandenname\bpmbxblist.csv]
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Laufweg Datei</label>
                                                <input type="file" name="uploadTrack"
                                                       id="uploadTrack" required>
                                            </div>
                                            <div class="alert alert-danger" role="alert">
                                                !!! <b>Bsp.</b> Stadtgarten Hotspots_Proband11_20160719T145430+0200.kmz
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>movisensApp Datei</label>
                                                <input type="file" name="uploadApp"
                                                       id="uploadApp" required>
                                            </div>
                                            <div class="alert alert-danger" role="alert">
                                                z.B Stadtgarten Hotspots_Proband11_20160719T150210+0200.csv
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Hotspot Bilder</label>
                                                <input type="file" multiple name="uploadBilder[]"
                                                       id="uploadBilder" required>
                                            </div>
                                            <div class="alert alert-danger" role="alert">
                                                alle bilder für den o.g Proband in einer selektion mit
                                                original name z.B. [movisensxs_Hotspots
                                                Stadtgarten\Proband2_ID11\12514513453.jpg]
                                                [movisensxs_HotspotsStadtgarten\Proband2_ID11\12514513454.jpg] ...
                                            </div>
                                        </div>

                                    </div>
                                    <div align="center">
                                        <button type="submit" class="btn btn-primary btn-block"
                                                name="submitUpload">Upload
                                        </button>
                                    </div>
                                </div>
                                <!-- /row-->
                            </div>
                            <!-- /.panel body -->
                        </div>
                        <!-- /.panel-default -->
                    </div>
                    <!-- /col-8 -->
                </form>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h7><b>Aktuelle Probanden in Hotspot</b></h7>
                        </div>
                        <div class="panel-body" id="prob-names">
                            <div id="list-view">
                                <ul>
                                    <?php
                                    $db = DB_Connection::getConnectionInstance();
                                    // Use the View for any result. Update is automatically
                                    $names = "SELECT * FROM pnames";
                                    $result = mysqli_query($db->getConnection(), $names);
                                    if (($result->num_rows) > 0) {
                                        // output data of each row
                                        while ($row = $result->fetch_assoc()) {
                                            echo "<li><a href='javascript:void(0);'>" . $row["unic_name"] . "</a></li>";
                                        }
                                    } else {
                                        echo "Keine Probanden";
                                    }
                                    $db->disconnect();
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <!-- panel body -->
                    </div>
                    <!-- / panel -default -->
                </div>
                <!-- /.col-lg-4 (nested) -->

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h7><b>Aktuelle Dateien in Hotspot</b></h7>
                        </div>
                        <div class="panel-body" id="prob-names">
                            <div id="list-view">
                                <ul>
                                    <?php
                                    // declare the path
                                    $pfad = "../probandenData/";

                                    function listAllFoldersAndFiles($dir)
                                    {
                                        $data = scandir($dir);
                                        foreach ($data as $item) {
                                            if (($item != ".") && ($item != "..")) {
                                                if (strpos((string)$item, 'png') !== false) {
                                                    continue;
                                                }
                                                echo "<li><a href='javascript:void(0);'>" . $item;
                                                if (is_dir($dir . "/" . $item)) {
                                                    echo "<ol>";
                                                    listAllFoldersAndFiles($dir . "/" . $item);
                                                    echo "</ol>";
                                                }
                                                echo "</a></li>";
                                            }
                                        }
                                        }

                                    // call the Function
                                    listAllFoldersAndFiles($pfad);
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <!-- panel body -->
                    </div>
                    <!-- / panel -default -->
                </div>
                <!-- /.col-lg-4 (nested) -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="../vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<script type="text/javascript">
    function ShowUploading(e) {
        var div = document.createElement('div');
        div.style.cssText = 'position: fixed; top: 20%; left: 40%; z-index: 5000; ' +
            'height:300px; width: 480px; text-align: center; background: #F5F5F5; border: 1px solid #000';
        var img = new Image();
        img.src = "../images/wait.gif";
        div.innerHTML = '<img src=' + '../images/wait.gif' + '> </br>Uploading...';

        document.body.appendChild(div);
        return true;
        // These 2 lines cancel form submission, so only use if needed.
        //window.event.cancelBubble = true;
        //e.stopPropagation();
    }
</script>

</body>

</html>

<!--
// Call the Class RecursiveIterator
$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pfad,
RecursiveDirectoryIterator::SKIP_DOTS),
RecursiveIteratorIterator::SELF_FIRST,
RecursiveIteratorIterator::CATCH_GET_CHILD);
$folders = array();
$files = array();
foreach ($objects as $name => $folder)
{
$folders[$name] = $folder;
if (is_dir($pfad.$folders[$name]))
{
echo "<li><a href='javascript:void(0);'>" . basename($name) . "</a></li>";
}
foreach ($folder as $name => $item)
{
$files[$name] = $item;
if ($handler = opendir($pfad.$folders[$name]))
{
while(false !== ($entry = readdir($handler)) )
{
echo "<li><a href='javascript:void(0);'>" . basename($entry) . "</a></li>";
}
}
}
}
-->