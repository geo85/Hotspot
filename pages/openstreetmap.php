﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>


    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../dist/css/progress-wizard.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- load leaflet.css -->
    <link href="../vendor/leafletCss/leaflet.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style>
        #oSmap {
            height: 350px
        }

        .legend i {
            width: 15px;
            height: 15px;
            float: left;
            margin-right: 8px;
            opacity: 0.7;
            border-radius: 15px;
        }

        .amcharts-chart-div a {
            display: none !important;
        }

        #gaugeChart {
            width: 100%;
            height: 80px;
        }
    </style>

</head>

<body>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php"); ?>
    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Darstellung mit der OpenstreetMap</h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row header tittle-->

        <!-- row welcome text-->
        <div class="row">
            <!-- welcome text col-lg-12-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Hotspot-Heatmap zu den Probanden
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="map-container">
                            <div id="oSmap"></div>
                            <div id="gaugeChart"></div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.welcome tet col-lg-12 -->
        </div>
        <!-- /.row welcome text-->

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper --

<!--
    <script src="../plugins/L.Path.Drag.js"></script>
    -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
<script src="https://unpkg.com/heatmap.js@2.0.5"></script>
<script src="https://unpkg.com/leaflet-heatmap@1.0.0"></script>


<script src="../js/amcharts/amcharts.js"></script>
<script src="../js/amcharts/serial.js"></script>
<script src="../js/amcharts/themes/light.js"></script>

<!--<!-- loading open street oSmap -->
<script type="text/javascript">

    setProbNamesOS();
    var probNamesAll = getPNamesOS();
    //console.log(probNamesAll);
    setProbTrackHeatOS(probNamesAll);
    var allProbTrackHeat = getTracksHeatOS();
    //console.log(allProbTrackHeat);

    //Verpflichtende Quellenangaben
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, Imagery &copy; <a href="https://icons8.com/web-app/7880/Marker-Filled"> Marker filled icon credits</a> ',
        maxZoom: 22
        }
    );

    var refs = new L.LayerGroup();

    var cfg = {
        // radius should be small ONLY if scaleRadius is true (or small radius is intended)
        // if scaleRadius is false it will be the constant radius used in pixels
        "radius": 0.0002,
        "maxOpacity": .8,
        // scales the radius based on map zoom
        "scaleRadius": true,
        // if set to false the heatmap uses the global maximum for colorization
        // if activated: uses the data maximum within the current map boundaries
        //   (there will always be a red spot with useLocalExtremas true)
        "useLocalExtrema": true,
        // which field name in your data represents the latitude - default "lat"
        latField: 'lat',
        // which field name in your data represents the longitude - default "lng"
        lngField: 'lng',
        // which field name in your data represents the data value - default "value"
        valueField: 'count'
    };


    var heatmapLayer = new HeatmapOverlay(cfg);

    var heatDaten = {
        max: 8,
        data: <?php setData(); ?>
    };

    var map = new L.Map('oSmap', {
        center: new L.LatLng(48.7815, 9.174),
        zoom: 17,
        layers: [baseLayer, refs, heatmapLayer]
    });


    heatmapLayer.setData(heatDaten);
    function getLayers() {
        var layers = [];
        for (var i = 0; i < allProbTrackHeat.length; i++) {

            var trackData = allProbTrackHeat[i].track;
            var polyLineArr = [];
            for (var j = 0; j < trackData.length; j++) {
                polyLineArr.push([parseFloat(trackData[j].lat.slice(0, 13)), parseFloat(trackData[j].lng.slice(0, 13))])
            }
            layers[i] = new L.LayerGroup();
            var polyline = new L.polyline(polyLineArr, {
                color: getRandomColor(),
                fillColor: getRandomColor(),
                fillOpacity: 0.5,
                weight: 4
            });
            polyline.addTo(layers[i]);

        }
        return layers;
    }
    var allLayers = new L.LayerGroup(getLayers()).addTo(map);


    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    function setProbNamesOS() {
        $.ajax({
            url: '../logic/probTracks.php',
            type: 'POST',
            data: {allProbsNammmmes: 'allProbsNammmmes'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                setPNamesOS(data);
            }
        });
    }

    var pNamesOS = [];
    function setPNamesOS(data) {
        var pNamesTempOS = [];
        for (var i = 0; i < data.length; i++) {
            pNamesTempOS[i] = data[i].unic_name;
        }
        pNamesOS = pNamesTempOS;
    }

    function getPNamesOS() {
        return pNamesOS;
    }

    function setProbTrackHeatOS(probNameTrack) {
        $.ajax({
            url: '../logic/probTracks.php',
            type: 'POST',
            data: {probNameTrack: probNameTrack},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                setTracksHeatOS(data);
            }
        });

    }

    var tracksProbHeatOs = [];
    function setTracksHeatOS(data) {
        tracksProbHeatOs = data;

    }

    function getTracksHeatOS() {
        return tracksProbHeatOs;
    }

    var gaugeChart = AmCharts.makeChart("gaugeChart", {
        "type": "serial",
        "rotate": true,
        "theme": "light",
        "autoMargins": false,
        "marginTop": 30,
        "marginLeft": 80,
        "marginBottom": 30,
        "marginRight": 50,
        "dataProvider": [{
            "category": "Heat-Bwrt.",
            "sehr-schlecht": 1,
            "schlecht": 2,
            "bfredigend": 3,
            "durchschnittlich": 4,
            "gut": 5,
            "sehr-gut": 6,
            "limit": 5,
            "perfekt": 7,
            "excelent": 8,
        }],
        "valueAxes": [{
            "minimum": 0,
            "maximum": 8,
            "stackType": "regular",
            "gridAlpha": 0
        }],
        "startDuration": 1,
        "graphs": [{
            "valueField": "excelent",
            "showBalloon": false,
            "type": "column",
            "lineAlpha": 0,
            "fillAlphas": 0.8,
            "fillColors": ["#19d228", "#f6d32b", "#fb2316"],
            "gradientOrientation": "horizontal",
        }],
        "columnWidth": 1,
        "categoryField": "category",
        "categoryAxis": {
            "gridAlpha": 0,
            "position": "left"
        }
    });

</script>

</body>
</html>

<?php
include_once "../logic/DB_Connection.php";

/**
 *
 * This function retrieve the data from the database
 * than we call this function with javascript
 * @since version 1.0
 */
function setData()
{
    $db = DB_Connection::getConnectionInstance();
    $names = "SELECT * FROM papp ";
    $result = mysqli_query($db->getConnection(), $names);
    $osData = [];
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $bewertung = $row["hotspot_bewertung"];
            $bewertung_int = (int)$bewertung;
            $bewertung_int = $bewertung_int + 20;
            $pic = $row["bild_name"];
            $lng = $row["longtitude"];
            $lat = $row["latitude"];
            $nonenc = $row["hotspot_name"];
            $hashname = mb_convert_encoding($nonenc, "UTF-8", "ISO-8859-1");
            $osData[$i] = "lat: " . $lat . "," . "lng: " . $lng . "," . "count: " . $bewertung_int;

            $i++;
            //setData($osData);
        }
        $itemsInArray = 0;
        $countItems = count($osData);
        echo '[';
        foreach ($osData as $item) {
            echo '{' . $item . '},';
            // see if the element of the array is the last index
            // if so than remove the ',' to have better array in javascript call
            if (++$itemsInArray === $countItems) {
                echo '{' . $item . '}';
            }
        }
        echo ']';
    } else {
        echo "Keine Hashtags";
    }
    $db->disconnect();
}

?>﻿
