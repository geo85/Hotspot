<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    -->
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link href="../dist/css/progress-wizard.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->


    <script src="../js/amcharts/amcharts.js"></script>
    <script src="../js/amcharts/serial.js"></script>
    <script src="../js/amcharts/themes/light.js"></script>

    <style>
        .amcharts-chart-div a {
            display: none !important;
        }

        #map-container {
            border: 0px solid transparent;
            background-color: #FFF;
            height: 360px;
            width: auto;
            position: relative;
        }

        #prob-names {
            z-index: 10;
            height: 100%;
            max-height: 501px;
            overflow-y: scroll;
        }

        #list-view h7 {
            font: 400 20px/1.5 Helvetica, Verdana, sans-serif;
            margin: 0;
            padding: 0;
        }

        #list-view ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #list-view ol {
            margin: 8px;
            padding: 2px;
            list-style-type: none;
        }

        #list-view li {
            font: 200 15px/1.5 Helvetica, Verdana, sans-serif;
            border-bottom: 1px solid #ccc;
        }

        #list-view li:last-child {
            border: none;
        }

        #list-view li a {
            text-decoration: none;
            color: #000;

            -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
            -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
            -o-transition: font-size 0.3s ease, background-color 0.3s ease;
            -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
            transition: font-size 0.3s ease, background-color 0.3s ease;
            display: block;
            width: auto;
        }

        #list-view li a:hover {
            font-size: 17px;
            background: #f6f6f6;
        }

        #gaugeChart {
            width: 100%;
            height: 80px;
        }

        #maindiv {
            height: 15px;
            width: 75%;
            float: right;
            position: relative;
            top: -25px;
            left: 25px;
        }
    </style>
</head>

<body>
<script src="../js/googleMap.js"></script>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php") ?>
    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h5 class="panel-heading">Darstellung mit der Openstreet-Map API</h5>
            </div>
            <!-- /.col-lg-12 -->

            <!-- /.row header tittle-->


            <!-- welcome text col-lg-12-->
            <div class="col-lg-7">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Hotspots aller Probanden
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="map-container"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <div class="col-lg-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Hotspot-Bild
                    </div>
                    <div class="panel-body">
                        <img id="pic" style="max-width: 100%;max-height: 350px;height: 350px;"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Hotspot-Bewertung
                    </div>
                    <div class="panel-body" style="height: 100px">
                        <div id="gaugeChart"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->


</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<script>

</script>
<script type="text/javascript">
    showHotBewertung();
    var hsData = getHot();
    var notCalled = true;


    var gaugeChart = AmCharts.makeChart("gaugeChart", {
        "type": "serial",
        "rotate": true,
        "theme": "light",
        "autoMargins": false,
        "marginTop": 30,
        "marginLeft": 80,
        "marginBottom": 30,
        "marginRight": 50,
        "dataProvider": [{
            "category": "Bewertung",
            "sehr-schlecht": 1,
            "schlecht": 2,
            "bfredigend": 3,
            "durchschnittlich": 4,
            "gut": 5,
            "sehr-gut": 6,
            "limit": 5,
            "perfekt": 7,
            "excelent": 8,
        }],
        "valueAxes": [{
            "minimum": 0,
            "maximum": 8,
            "stackType": "regular",
            "gridAlpha": 0
        }],
        "startDuration": 1,
        "graphs": [{
            "valueField": "excelent",
            "showBalloon": false,
            "type": "column",
            "lineAlpha": 0,
            "fillAlphas": 0.8,
            "fillColors": ["#fb2316", "#f6d32b", "#19d228"],
            "gradientOrientation": "horizontal",
        }, {
            "clustered": false,
            "columnWidth": 0,
            "fillAlphas": 0,
            "lineColor": "#",
            "stackable": false,
            "type": "column",
            "bullet": "triangleDown",
            "bulletSize": 20,
            "valueField": "limit"
        }],
        "columnWidth": 1,
        "categoryField": "category",
        "categoryAxis": {
            "gridAlpha": 0,
            "position": "left"
        }
    });




    function showHotBewertung() {
        console.log("ajax rockt");
        $.ajax({
            url: '../logic/showGauge.php',
            type: 'POST',
            data: {hot: 'hot'},
            dataType: 'JSON',
            async: false,
            success: function (data) {

                setHot(data);


            }
        });
    }


    var rohData = [];
    function setHot(hotData) {
        rohData = hotData;
    }

    function getHot() {
        return rohData;
    }
    var markers = [];
    function setMarkers(mrkrs) {
        markers = mrkrs;
    }
    function getMarkers() {
        return markers;

    }

    function createMarkers() {
        if (notCalled) {
            var markers = [];
            map.setZoom(16);
            for (var i = 0; i < hsData.length; i++) {
                var latLang = new google.maps.LatLng(hsData[i].latitude, hsData[i].longtitude);


                var icon = {
                    url: "../images/marker_" + hsData[i].hotspot_bewertung + ".png", // url
                    scaledSize: new google.maps.Size(25, 25), // scaled size
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };
                markers[i] = new google.maps.Marker({
                    position: latLang,
                    map: map,
                    icon: icon,
                    //animation: google.maps.Animation.BOUNCE
                });
            }
            setMarkers(markers);
            notCalled = false;

        }

    }


    window.setInterval(function () {
        createMarkers();

        var markerTemp;
        for (var i = 0; i < hsData.length; i++) {
            if (markers[i].getAnimation() != null) {
                markers[i].setAnimation(null);
            }
        }


        var rand = Math.floor(Math.random() * hsData.length);

        markers[rand].setAnimation(google.maps.Animation.BOUNCE);
        console.log(rand);
        var img = document.createElement("img");
        var src = '../probandenData/images/' + hsData[rand].bild_name;

        console.log(img.src);
        document.getElementById("pic").src = src;
        var dataP = [{
            "category": "Bewertung",
            "sehr-schlecht": 1,
            "schlecht": 2,
            "bfredigend": 3,
            "durchschnittlich": 4,
            "gut": 5,
            "sehr-gut": 6,
            "limit": hsData[rand].hotspot_bewertung,
            "perfekt": 7,
            "excelent": 8,
            "wow": 9

        }];
        gaugeChart.dataProvider = dataP;
        gaugeChart.validateData();
    }, 5000);


</script>
</body>

</html>
<?php
function setDataOs()
{
    $db = DB_Connection::getConnectionInstance();
    $names = "SELECT * FROM papp ";
    $result = mysqli_query($db->getConnection(), $names);
    $osData = [];
    $i = 0;
    if ($result->num_rows > 0) {
// output data of each row
        while ($row = $result->fetch_assoc()) {
            $bewertung = $row["hotspot_bewertung"];
            $bewertung_int = (int)$bewertung;
            $bewertung_int = $bewertung_int + 20;
            $pic = $row["bild_name"];
            $lng = $row["longtitude"];
            $lat = $row["latitude"];
            $nonenc = $row["hotspot_name"];
            $hashname = mb_convert_encoding($nonenc, "UTF-8", "ISO-8859-1");
            $osData[$i] = "lat: " . $lat . "," . "lng: " . $lng . "," . "count: " . $bewertung_int;

            $i++;
//setData($osData);
        }
        $itemsInArray = 0;
        $countItems = count($osData);
        echo '[';
        foreach ($osData as $item) {
            echo '{' . $item . '},';
// see if the element of the array is the last index
// if so than remove the ',' to have better array in javascript call
            if (++$itemsInArray === $countItems) {
                echo '{' . $item . '}';
            }
        }
        echo ']';
    } else {
        echo "Keine Hashtags";
    }
    $db->disconnect();
}
?>﻿



