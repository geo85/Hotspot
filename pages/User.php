﻿<?php

class User
{

    private $firstname;
    private $lastname;
    private $email;
    private $password;

    /**
     * User constructor.
     * @param $fname
     * @param $lname
     * @param $mail
     * @param $pass
     */
    public function __construct($fname, $lname, $mail, $pass)
    {
        $this->firstname = $fname;
        $this->lastname = $lname;
        $this->email = $mail;#
        $this->password = $pass;


    }

    /**
     * @param $name
     *
     * @return set name for user
     *
     * @since version
     */
    public function setFirstName($name)
    {
        return $this->firstname = $name;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = crypt($password); // password encryption
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
}

?>