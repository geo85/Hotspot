﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    -->
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Load c3.css -->
    <link href="../dist/css/c3.css" rel="stylesheet" type="text/css">

    <!-- Load d3.js and c3.js -->
    <script src="../js/d3.v3.min.js" charset="utf-8"></script>
    <script src="../js/c3.min.js"></script>
    <script src="../js/moment.js"></script>
</head>
<style>
    .c3-axis-y2 {
        fill: #141414;
        stroke: #141414;

    }

    .c3-axis-y2-label {

        fill: #141414;

    }

    .c3-axis-y2 .tick {
        fill: #141414;
    }
</style>
<body>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php") ?>
    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">App-rating</small></h1>
            </div>
            <!-- /.col-lg-12 -->

        <!-- /.row header tittle-->

        <!-- row welcome text-->

            <!-- welcome text col-lg-12-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Probanden Stimmungen
                    </div>
                    <div class="col-lg-5">
                        <h4>Eintreten</h4><br>
                        <div id="chartsHolderEnter"></div>
                    </div>
                    <div class="col-lg-2">
                        <h4>HotSpot</h4><br>
                        <div id="chartsHolderHotspot"></div>
                    </div>
                    <div class="col-lg-5">
                        <h4>Austreten</h4><br>
                        <div id="chartsHolderExit"></div>
                    </div>
                    <div class="col-lg-6">
                        <h4>Frequenz aller Probanden beim Eintreten</h4><br>
                        <div id="allHeartEnterDiv"></div>
                    </div>
                    <div class="col-lg-6">
                        <h4>Frequenz aller Probanden beim Austreten</h4><br>
                        <div id="allHeartExitDiv"></div>
                    </div>

                    <!-- /.panel-body -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Glücklich Eintreten
                        </div>

                        <div id="gluecklichEnter"></div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Glücklich Austreten
                        </div>
                        <!-- /.panel-heading -->

                        <div id="gluecklichExit"></div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Entspannt Eintreten
                        </div>

                        <div id="entspanntEnter"></div>

                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Entspannt Austreten
                        </div>

                        <div id="entspanntExit"></div>

                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Unglücklich Eintreten
                        </div>

                        <div id="ungluecklichEnter"></div>


                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Unglücklich Austreten
                        </div>

                        <div id="ungluecklichExit"></div>

                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Angespannt Eintreten
                        </div>

                        <div id="angespanntEnter"></div>

                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Angespannt Austreten
                        </div>

                        <div id="angespanntExit"></div>

                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Gelassen Eintreten
                        </div>
                        <div id="gelassenEnter"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Gelassen Austreten
                        </div>
                        <div id="gelassenExit"></div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Nervös Eintreten
                        </div>
                        <div id="nervoesEnter"></div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Nervös Austreten
                        </div>
                        <div id="nervoesExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Munter Eintreten
                        </div>
                        <div id="munterEnter"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Munter Austreten
                        </div>
                        <div id="munterExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Müde Eintreten
                        </div>
                        <div id="muedeEnter"></div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Müde Austreten
                        </div>
                        <div id="muedeExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Wach Eintreten
                        </div>
                        <div id="wachEnter"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Wach Austreten
                        </div>
                        <div id="wachExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Schlapp Eintreten
                        </div>
                        <div id="schlappEnter"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Schlapp Austreten
                        </div>
                        <div id="schlappExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Unzufrieden Eintreten
                        </div>
                        <div id="zufriedenEnter"></div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Zufrieden Austreten
                        </div>
                        <div id="zufriedenExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Unzufrieden Eintreten
                        </div>
                        <div id="unzufriedenEnter"></div>
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Unzufrieden Austreten
                        </div>
                        <div id="unzufriedenExit"></div>
                    </div>
                    <!-- /.panel -->
                </div>
            <!-- /.welcome tet col-lg-12 -->
        </div>
        <!-- /.row welcome text-->
        </div>
        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->


<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<script type="text/javascript">
    var goodColors = ['red', 'orange', 'yellow', 'YellowGreen ', 'green'];
    var badColors = ['green', 'YellowGreen ', 'yellow', 'orange', 'red'];
    getHostpotJSON();
    setAllExitData();
    setAllEnterData();
    setHeartEnterData();
    setHeartExitData();
    setHeartEnterArr();
    setHeartExitArr();
    showEnterCharts();
    showExitCharts();
    drawHeartChartEnter();
    drawHeartChartExit();


    function showEnterCharts() {
        for (var i = 0; i < getAllEnter().length; i++) {
            var pname = getAllEnter()[i].pName;
            var freq = getHeartEnter().filter(function (obj) {
                if (obj.pName == pname) {
                    return obj.freq;
                }
            });
            //console.log(freq);
            var iDiv = document.createElement('div');
            var iDivHotSot = document.createElement('div');
            iDiv.id = 'chartEnter' + pname;
            iDivHotSot.id = 'chartHostspot' + pname;
            document.getElementById('chartsHolderEnter').appendChild(iDiv);
            document.getElementById('chartsHolderHotspot').appendChild(iDivHotSot);
            var chart = c3.generate({
                bindto: "#chartEnter" + pname,
                data: {
                    columns: [
                        ['angespannt', getAllEnter()[i].angespannt],
                        ['entspannt', getAllEnter()[i].entspannt],
                        ['gelassen', getAllEnter()[i].gelassen],
                        ['gluecklich', getAllEnter()[i].gluecklich],
                        ['muede', getAllEnter()[i].muede],
                        ['munter', getAllEnter()[i].munter],
                        ['nervoes', getAllEnter()[i].nervoes],
                        ['schlapp', getAllEnter()[i].schlapp],
                        ['ungluecklich', getAllEnter()[i].ungluecklich],
                        ['unzufrieden', getAllEnter()[i].unzufrieden],
                        ['wach', getAllEnter()[i].wach],
                        ['zufrieden', getAllEnter()[i].zufrieden],
                        ['freq', freq[0].freq]
                    ],
                    axes: {
                        freq: 'y2'
                    },
                    type: 'bar',
                    colors: {
                        freq: '#141414'
                    }
                },
                axis: {
                    x: {
                        label: {
                            text: pname,
                            position: 'outer-center'
                        },
                        type: 'category'
                    },
                    y: {
                        label: {
                            text: 'Stimmung skala 1-5',
                            position: 'outer-middle'
                        },
                        type: 'value'
                    },
                    y2: {
                        show: true,
                        label: {
                            text: 'Frequenz',
                            position: 'outer-middle'
                        }
                    }
                }
            });
            //console.log(pname, getHotSpotForProband(pname));
            var chartHotspot = c3.generate({
                bindto: "#chartHostspot" + pname,
                data: {
                    columns: getHotSpotForProband(pname),
                    type: 'bar'
                },
                axis: {
                    x: {
                        label: {
                            text: pname,
                            position: 'outer-center'
                        },
                        type: 'category'
                    },
                    y: {
                        label: {
                            text: 'Bewertung skala 0-8 ',
                            position: 'outer-middle'
                        },
                        type: 'value'
                    }
                }
            });
        }
    }

    function showExitCharts() {

        for (var i = 0; i < getAllExit().length; i++) {
            var pname = getAllExit()[i].pName;
            var freq = getHeartExit().filter(function (obj) {
                if (obj.pName == pname) {
                    return obj.freq;
                }
            });
            var iDiv = document.createElement('div');

            iDiv.id = 'chartExit' + pname;
            document.getElementById('chartsHolderExit').appendChild(iDiv);
            var chart = c3.generate({
                bindto: "#chartExit" + pname,

                data: {
                    columns: [
                        ['angespannt', getAllExit()[i].angespannt],
                        ['entspannt', getAllExit()[i].entspannt],
                        ['gelassen', getAllExit()[i].gelassen],
                        ['gluecklich', getAllExit()[i].gluecklich],
                        ['muede', getAllExit()[i].muede],
                        ['munter', getAllExit()[i].munter],
                        ['nervoes', getAllExit()[i].nervoes],
                        ['schlapp', getAllExit()[i].schlapp],
                        ['ungluecklich', getAllExit()[i].ungluecklich],
                        ['unzufrieden', getAllExit()[i].unzufrieden],
                        ['wach', getAllExit()[i].wach],
                        ['zufrieden', getAllExit()[i].zufrieden],
                        ['freq', freq[0].freq]
                    ],
                    type: 'bar',

                    axes: {
                        freq: 'y2'
                    },
                    colors: {
                        freq: '#141414'
                    }
                },
                axis: {
                    x: {
                        label: {
                            text: pname,
                            position: 'outer-center',
                            type: 'category'
                        }
                    },
                    y: {
                        label: {
                            text: 'Stimmung skala 1-5',
                            position: 'outer-middle',
                            type: 'value'
                        }
                    },
                    y2: {
                        show: true,
                        label: {
                            text: 'Frequenz',
                            position: 'outer-middle'
                        }
                    }
                }
            });
        }
    }

    function drawHeartChartEnter() {
        var data = [];
        data.push(getHeartEnterProbNameArr());
        data.push(getHeartEnterArr());
        //console.log(data);
        var hearChart = c3.generate({
            bindto: '#allHeartEnterDiv',
            data: {
                x: 'probNames',
                columns: data,
                type: 'line'
            },
            axis: {
                x: {
                    label: {
                        text: 'probanden',
                        position: 'outer-center'
                    },
                    type: 'category'
                },
                y: {
                    label: {
                        text: 'Frequenz',
                        position: 'outer-middle',
                        type: 'value'
                    }
                }
            }
        });
    }

    function drawHeartChartExit() {
        var data = [];
        data.push(getHeartExitArr());
        data.push(getHeartExitProbNameArr());
        //console.log(data);
        var hearChart = c3.generate({
            bindto: '#allHeartExitDiv',
            data: {
                x: 'probNames',
                columns: data
            },
            type: 'line',
            axis: {
                x: {
                    label: {
                        text: 'pName',
                        position: 'outer-center'
                    },
                    type: 'category'
                },
                y: {
                    label: {
                        text: 'Frequenz',
                        position: 'outer-middle',
                        type: 'value'
                    }
                }
            }
        });
    }

    //console.log(getAllExit());

    setExitData();
    setEnterData();

    createChart(getEnter(), 'gluecklich', 'gluecklichEnter', goodColors, 'bar');
    createChart(getExit(), 'gluecklich', 'gluecklichExit', goodColors, 'bar');
    createChart(getEnter(), 'entspannt', 'entspanntEnter', goodColors, 'bar');
    createChart(getExit(), 'entspannt', 'entspanntExit', goodColors, 'bar');

    createChart(getEnter(), 'ungluecklich', 'ungluecklichEnter', badColors, 'pie');
    createChart(getExit(), 'ungluecklich', 'ungluecklichExit', badColors, 'pie');
    createChart(getEnter(), 'angespannt', 'angespanntEnter', badColors, 'pie');
    createChart(getExit(), 'angespannt', 'angespanntExit', badColors, 'pie');

    createChart(getEnter(), 'wach', 'wachEnter', goodColors, 'bar');
    createChart(getExit(), 'wach', 'wachExit', goodColors, 'bar');
    createChart(getEnter(), 'munter', 'munterEnter', goodColors, 'bar');
    createChart(getExit(), 'munter', 'munterExit', goodColors, 'bar');

    createChart(getEnter(), 'schlapp', 'schlappEnter', badColors, 'pie');
    createChart(getExit(), 'schlapp', 'schlappExit', badColors, 'pie');
    createChart(getEnter(), 'muede', 'muedeEnter', badColors, 'pie');
    createChart(getExit(), 'muede', 'muedeExit', badColors, 'pie');

    createChart(getEnter(), 'nervoes', 'nervoesEnter', badColors, 'bar');
    createChart(getExit(), 'nervoes', 'nervoesExit', badColors, 'bar');
    createChart(getEnter(), 'gelassen', 'gelassenEnter', goodColors, 'bar');
    createChart(getExit(), 'gelassen', 'gelassenExit', goodColors, 'bar');

    createChart(getEnter(), 'zufrieden', 'zufriedenEnter', goodColors, 'pie');
    createChart(getExit(), 'zufrieden', 'zufriedenExit', goodColors, 'pie');
    createChart(getEnter(), 'unzufrieden', 'unzufriedenEnter', badColors, 'pie');
    createChart(getExit(), 'unzufrieden', 'unzufriedenExit', badColors, 'pie');


    function setAllEnterData() {
        $.ajax({
            url: '../logic/enterExitGarden.php',
            type: 'POST',
            data: {penterAll: 'penterAll'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                setAllEnter(data);
            }
        });
    }

    var enterAllData = [];
    function setAllEnter(enterD) {
        enterAllData = enterD;
    }

    function getAllEnter() {
        return enterAllData;
    }

    function setAllExitData() {
        $.ajax({
            url: '../logic/enterExitGarden.php',
            type: 'POST',
            data: {pexitAll: 'pexitAll'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                setAllExit(data);
            }
        });
    }

    var exitAllData = [];
    function setAllExit(enterD) {
        exitAllData = enterD;
    }

    function getAllExit() {
        return exitAllData;
    }

    function setEnterData() {
        $.ajax({
            url: '../logic/enterExitGarden.php',
            type: 'POST',
            data: {penter: 'penter'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                setEnter(data);
            }
        });
    }

    var enterData = [];
    function setEnter(enterD) {
        enterData = enterD;
    }

    function getEnter() {
        return enterData;
    }

    function setExitData() {
        $.ajax({
            url: '../logic/enterExitGarden.php',
            type: 'POST',
            data: {pexit: 'pexit'},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                setExit(data);
            }
        });
    }

    var exitData = [];
    function setExit(exitD) {

        exitData = exitD;
    }

    function getExit() {
        return exitData;
    }

    function createChart(dataToPrepare, adj, div, colors, type) {
        var data = prepareData(dataToPrepare, adj);


        var chartEnter = c3.generate({
            bindto: "#" + div,
            data: {

                columns: data,

                type: type,

                types: {
                    freq: 'line',
                }


            },
            axes: {
                freq: 'y2',
                probNames: 'x2'
            },
            pie: {
                label: {
                    format: function (value, ratio, id) {
                        return value;
                    }
                }
            },
            axis: {
                x: {
                    label: {
                        text: adj + ' Stimmung skala 1-5',
                        position: 'outer-center',


                    },
                    type: 'category',
                },
                y: {
                    label: {
                        text: 'Anzahl Probanden',
                        position: 'outer-middle',

                    },
                    type: 'value'
                }
            },
            color: {
                pattern: colors
            },
            bar: {
                width: {
                    ratio: 0.5 // this makes bar width 50% of length between ticks
                }
            }
        });
    }

    function prepareData(dataToPrepare, adj) {
        var columns = [];
        for (var i = 1; i <= 5; i++) {
            var count = 0;
            var column = [];
            column.push(i);

            for (var j = 0; j < dataToPrepare.length; j++) {

                var obj = dataToPrepare[j];
                for (var k in obj) {
                    if (k.toString() === adj.toString()) {
                        if (parseInt(obj[k]) === i) {
                            count++;

                        }
                    }

                }

            }
            column.push(count);
            columns.push(column);
        }
        return columns;
    }

    function setHeartEnterData() {
        $.ajax({
            url: '../logic/enterExitGarden.php',
            type: 'POST',
            data: {penterHeart: 'penterHeart'},
            dataType: 'JSON',
            async: false,
            success: function (data) {

                setHeartEnter(data);
            }
        });
    }

    var heartEnterData = [];
    function setHeartEnter(hEnterD) {

        heartEnterData = hEnterD;
    }

    function getHeartEnter() {
        return heartEnterData;
    }

    function setHeartExitData() {
        $.ajax({
            url: '../logic/enterExitGarden.php',
            type: 'POST',
            data: {pexitHeart: 'pexitHeart'},
            dataType: 'JSON',
            async: false,
            success: function (data) {

                setHeartExit(data);
            }
        });
    }

    var heartExitData = [];
    function setHeartExit(hexitD) {

        heartExitData = hexitD;
    }

    function getHeartExit() {
        return heartExitData;
    }

    var heartEnterArr = [];
    var heartEnterProbNames = [];

    function setHeartEnterArr() {
        var heartTmpEnterArr = [];
        var heartTmpEnterProbNames = [];
        heartTmpEnterArr[0] = "freq";
        heartTmpEnterProbNames[0] = "probNames";
        for (var i = 0; i < getHeartEnter().length; i++) {
            heartTmpEnterArr.push(parseInt(getHeartEnter()[i].freq));
            heartTmpEnterProbNames.push(getHeartEnter()[i].pName);
        }
        heartEnterArr = heartTmpEnterArr;
        heartEnterProbNames = heartTmpEnterProbNames;
    }

    var heartExitArr = [];
    var heartExitProbNames = [];
    function setHeartExitArr() {
        var heartTmpExitArr = [];
        var heartTmpExitProbNames = [];
        heartTmpExitArr[0] = "freq";
        heartTmpExitProbNames[0] = "probNames";
        for (var i = 0; i < getHeartExit().length; i++) {
            heartTmpExitArr.push(parseInt(getHeartExit()[i].freq));
            heartTmpExitProbNames.push(getHeartExit()[i].pName);
        }
        heartExitArr = heartTmpExitArr;
        heartExitProbNames = heartTmpExitProbNames;
    }

    function getHeartEnterArr() {
        return heartEnterArr;
    }

    function getHeartExitProbNameArr() {
        return heartExitProbNames;
    }

    function getHeartEnterProbNameArr() {
        return heartEnterProbNames;
    }

    function getHeartExitArr() {
        return heartExitArr;
    }

    function getHostpotJSON() {

        $.ajax({
            url: '../logic/hotSpotter.php',
            data: '',
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                var hotspots = data;
                setHotS(data);
            }
        });
    }

    var hotSpotData = [];
    function setHotS(data) {
        hotSpotData = data;
    }

    function getHotS() {
        return hotSpotData;

    }

    function getHotSpotForProband(pname) {
        var probObj = getHotS().filter(function (obj) {
            return obj.pName == pname;
        });
        console.log(probObj);
        var probHotSpots = [];
        for (var i = 0; i < probObj.length; i++) {
            probHotSpots.push(['hotspot#' + (i + 1), probObj[i].hotspot_bewertung])

        }
        return probHotSpots;
    }



</script>

</body>

</html>



