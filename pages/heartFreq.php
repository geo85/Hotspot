﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    -->
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
    <!-- Load underscore -->
    <script type="text/javascript" src="../js/lodash.js"></script>
    <!-- Load c3.css -->
    <link href="../dist/css/c3.css" rel="stylesheet" type="text/css">

    <!-- Load d3.js and c3.js -->
    <script src="../js/d3.v3.min.js" charset="utf-8"></script>
    <script src="../js/c3.min.js"></script>
    <script src="../js/moment.js"></script>

    <style>
        #map-container {
            border: 0px solid transparent;
            background-color: #FFF;
            height: 360px;
            width: auto;
            position: relative;
        }

        #heartChart {
            width: auto;
            height: 200px;
        }

        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto', 'sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #floating-panel {
            background-color: #fff;
            border: 1px solid #999;
            left: 25%;
            padding: 5px;
            position: absolute;
            top: 10px;
            z-index: 5;
        }

        #prob-names {
            z-index: 10;
            height: 100%;
            max-height: 501px;
            overflow-y: scroll;
        }

        #list-view {
        }

        #list-view h7 {
            font: 400 20px/1.5 Helvetica, Verdana, sans-serif;
            margin: 0;
            padding: 0;
        }

        #list-view ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        #list-view ol {
            margin: 8px;
            padding: 2px;
            list-style-type: none;
        }

        #list-view li {
            font: 200 15px/1.5 Helvetica, Verdana, sans-serif;
            border-bottom: 1px solid #ccc;
        }

        #list-view li:last-child {
            border: none;
        }

        #list-view li a {
            text-decoration: none;
            color: #000;

            -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
            -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
            -o-transition: font-size 0.3s ease, background-color 0.3s ease;
            -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
            transition: font-size 0.3s ease, background-color 0.3s ease;
            display: block;
            width: auto;
        }

        #list-view li a:hover {
            font-size: 17px;
            background: #f6f6f6;
        }

        #map-container {
            border: 0px solid transparent;
            background-color: #FFF;
            height: 360px;
            width: auto;
            position: relative;
        }
    </style>
</head>

<body>
<script src="../js/googleMap.js"></script>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php") ?>
    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h5 class="panel-heading">Darstellung mit der Google-Map API</h5>
            </div>
            <!-- /.col-lg-12 -->

        <!-- /.row header tittle-->


            <!-- welcome text col-lg-12-->
            <div class="col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Laufwege der Probanden
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div id="map-container"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Probanden
                    </div>
                    <div class="panel-body" id="prob-names">
                        <div id="list-view">
                            <form method="post" action="" enctype="multipart/form-data" id="probForm">
                                <ul>
                                    <?php
                                    $db = DB_Connection::getConnectionInstance();
                                    // Use the View for any result. Update is automatically
                                    $names = "SELECT * FROM pnames";
                                    $result = mysqli_query($db->getConnection(), $names);
                                    if (($result->num_rows) > 0) {
                                        // output data of each row
                                        while ($row = $result->fetch_assoc()) {
                                            $pname = $row["unic_name"];
                                            echo '<li><input class="checkbox-inline" type="checkbox" 
                                            name="proband"
                                            value="' . $pname . '">' . $pname . '</input></li>';
                                        }
                                    } else {
                                        echo "Keine Probanden";
                                    }
                                    $db->disconnect();

                                    ?>
                                    <input type="submit" onclick="setSelectedProb()" value="Select"
                                           name="selectProbanden">
                                </ul>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-12"> Herz Frequenz
                <hr/>

                <div id="heartChart"></div>

            </div>


        </div>

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->


</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>


<script type="text/javascript">
    //show markers

    //c3
    var c3Chart;


    var trackData = [];
    var selectedProbanden = [];
    var probandenColors = [];
    function setSelectedProb() {

        getHotspots();
        var temp = [];
        $("input.checkbox-inline:checked").each(function () {
            temp.push($(this).val());

        });
        selectedProbanden = temp;
        console.log(getSelectedProb());
        //var trackData=getProbTrack(selectedProbanden);
        var tempTrack = [];

        setProbTrack(selectedProbanden);
        var trackData = getTracks();
        setProbandenColors(selectedProbanden);
        console.log(trackData);

        var colors = getProbandenColors();
        drawTrackLine(trackData, colors);
        setProbFreqs(selectedProbanden);
        var chartData = getFreqs();
        console.log(chartData);
        drawC3Chart(chartData, colors);

    }
    function getSelectedProb() {
        return selectedProbanden;
    }

    function getHotspots() {
        var hotspots;
        $.ajax({
            url: '../logic/hotSpotter.php',
            data: '',
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                hotspots = data;
                var markers = [];
                var positions = [];
                var infowindows = [];
                var contentString = [];
                console.log(hotspots);
                for (var i = 0; i < hotspots.length; i++) {
                    var marker = markers[i];
                    var infowindow = infowindows[i];
                    var icon = {
                        url: "../images/marker_" + hotspots[i].hotspot_bewertung + ".png", // url
                        scaledSize: new google.maps.Size(20, 20), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    };
                    var picSrc = "../probandenData/images/" + hotspots[i].bild_name;
                    contentString[i] = "Proband: <b>" + hotspots[i].pName + '</b><br>Hashtag: <b>' + hotspots[i].hotspot_name
                        + '</b><br><IMG BORDER="0" ALIGN="center" height="70 px" width="90 px" ' +
                        'SRC=' + picSrc + '> ';
                    infowindows[i] = new google.maps.InfoWindow({
                        maxWidth: 100
                    });
                    positions[i] = new google.maps.LatLng(hotspots[i].latitude
                        , hotspots[i].longtitude);
                    markers[i] = new google.maps.Marker({
                        position: positions[i],
                        map: map,
                        icon: icon
                        //animation: google.maps.Animation.BOUNCE
                    });
                    bindInfoWindow(markers[i], map, infowindows[i], contentString[i]);
                }
                setMarkers(markers);
            }

        });

    }
    function bindInfoWindow(marker, map, infowindow, description) {
        marker.addListener('click', function () {
            infowindow.setContent(description);
            infowindow.open(map, this);
        });
    }

    var markers=[];
    function setMarkers(mrkrs){
       markers=mrkrs;
    }
    function getMarkers() {
        return markers;
    }
    function setProbTrack(probNameTrack) {
        $.ajax({
            url: '../logic/probTracks.php',
            type: 'POST',
            data: {probNameTrack: probNameTrack},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                setTracks(data);
            }
        });

    }

    var tracksProb = [];
    function setTracks(data) {
        tracksProb = data;
    }
    function getTracks() {
        return tracksProb;
    }


    function setProbandenColors(probandenArray) {
        for (var i = 0; i < probandenArray.length; i++) {
            probandenColors[i] = getRandomColor();
        }
    }
    function getProbandenColors() {
        return probandenColors;
    }


    function setProbFreqs(probNameFreq) {
        $.ajax({
            url: '../logic/probFreqs.php',
            type: 'POST',
            data: {probNameFreq: probNameFreq},
            dataType: 'JSON',
            async: false,
            success: function (data) {
                //console.log(data); // Inspect this in your console
                setFreqs(data);
            }
        });

    }

    var probFreq = [];
    function setFreqs(data) {
        probFreq = data;
    }
    function getFreqs() {
        return probFreq;
    }

    var PathStyle = [];
    var PathStyleOld = [];
    var rectangle;
    var rectangleOld;
    function drawTrackLine(proobandenTracks, trackColor) {


        map.setZoom(16);
        for (var j = 0; j < proobandenTracks.length; j++) {
            var tempTrack = [];
            for (var k = 0; k < proobandenTracks[j].track.length; k++) {

                proobandenTracks[j].track[k].lat = parseFloat(proobandenTracks[j].track[k].lat);
                proobandenTracks[j].track[k].lng = parseFloat(proobandenTracks[j].track[k].lng);


            }

        }

        removeLine(PathStyleOld);
        addLines(proobandenTracks, trackColor);


        console.log(proobandenTracks.length);
        if (rectangleOld) {

            removeRectangle(rectangleOld);
        }
        addRectangle();
    }


    function removeRectangle(rect) {

        rect.setMap(null);
    }

    function addRectangle() {
        var bounds = {
            north: 48.780355,
            south: 48.780040,
            east: 9.1729000,
            west: 9.1722050
        };

        rectangle = new google.maps.Rectangle({
            bounds: bounds,
            editable: true,
            draggable: true
        });

        rectangle.setMap(map);

        rectangle.addListener('dragstart', unselectChartSelection);
        rectangle.addListener('dragend', showNewRectFreqs);
        rectangleOld = rectangle;
    }

    function unselectChartSelection() {
        stopJump();
        c3Chart.unselect();
    }

    function showNewRectFreqs(event) {
        // Define an info window on the map.

        var bounds = rectangle.getBounds();
        //console.log("START LOOP EVENT");
        var time = [];
        for (var i = 0; i < PathStyle.length; i++) {
            var arrPath = PathStyle[i].getPath();
            for (var j = 0; j < arrPath.length; j++) {
                var latLng = new google.maps.LatLng(arrPath.getAt(j).lat(), arrPath.getAt(j).lng());
                if (bounds.contains(latLng)) {
                    var probandNameFromPath = PathStyle[i].name;
                    //console.log(probandNameFromPath);
                    var startTime = PathStyle[i].startTime;
                    //console.log(startTime);
                    var latPath = arrPath.getAt(j).lat();
                    var lngPath = arrPath.getAt(j).lng();
                    //console.log("lat: "+latPath+" ,lng: "+lngPath);
                    time[j] = getFromProbandTime(probandNameFromPath, latPath, lngPath, startTime);
                    //console.log(time);
                }
            }
        }
        //console.log("END LOOP EVENT");
    }


    function addLines(proobandenTracks, trackColor) {
        for (var i = 0; i < proobandenTracks.length; i++) {

            PathStyle[i] = new google.maps.Polyline({
                path: proobandenTracks[i].track,
                strokeColor: trackColor[i],
                strokeOpacity: 1.0,
                name: proobandenTracks[i].proband,
                startTime: proobandenTracks[i].track[0].time,
                strokeWeight: 2
            });

            PathStyle[i].setMap(map);
        }
        PathStyleOld = PathStyle;
    }


    function removeLine(path) {
        if (path.length > 0) {
            for (var i = 0; i < path.length; i++) {
                path[i].setMap(null);
            }
        }


    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }


    function drawC3Chart(selectedProbanden, colors) {
        var probFreqs = [];
        var probTimes = [];
        for (var i = 0; i < selectedProbanden.length; i++) {
            var freq = [];
            var time = [];
            freq.push(selectedProbanden[i].proband);
            //time.push(selectedProbanden[i].proband);
            var arrLenth = selectedProbanden[i].freq.length;
            if (arrLenth > 700) {
                arrLenth = 700;
            }
            for (var k = 0; k < arrLenth; k++) {

                freq.push(parseInt(selectedProbanden[i].freq[k].freq));

                time.push(parseInt(selectedProbanden[i].freq[k].time));

            }
            probFreqs[i] = freq;
            probTimes[i] = time;
        }
        //console.log(probFreqs);
        //console.log(probTimes);
        c3Chart = c3.generate({
            bindto: "#heartChart",
            data: {
                selection: {
                    enabled: true,
                    multiple: true,
                    onselected: function (d) {
                        c3Chart.tooltip.show();
                    }
                },
                columns: probFreqs,

            },
            tooltip: {
                show: true
            },
            axis: {
                x: {
                    label: {
                        text: 'Zeit Einheiten (4 Sek.)',
                        position: 'outer-center',
                        type: 'category'
                    }
                },
                y: {
                    label: {
                        text: 'Frequenz',
                        position: 'outer-middle',
                        type: 'value'
                    }
                }
            },
            color: {
                pattern: colors
            }
        });

    }

    function getFromProbandTime(probandName, lat, lng, startTime) {

        var time;
        var probObj = getTracks().filter(function (obj) {
            return obj.proband === probandName;
        });
        var latStringFromEvent = lat.toString();
        var lngStringFromEvent = lng.toString();
        latStringFromEvent = latStringFromEvent.slice(0, 13);
        var latFloatFromEvent = parseFloat(latStringFromEvent);
        lngStringFromEvent = lngStringFromEvent.slice(0, 13);
        var lngFloatFromEvent = parseFloat(lngStringFromEvent);
        for (var j = 0; j < probObj.length; j++) {
            var arrTrack = probObj[j].track.filter(function (obj) {
                var latStringFromObj = obj.lat.toString();
                var lngStringFromObj = obj.lng.toString();

                latStringFromObj = latStringFromObj.slice(0, 13);
                var latFloatFromObj = parseFloat(latStringFromObj);
                lngStringFromObj = lngStringFromObj.slice(0, 13);
                var lngFloatFromObj = parseFloat(lngStringFromObj);
                //console.log(latStringFromObj);
                if (latFloatFromObj == latFloatFromEvent && lngFloatFromObj == lngFloatFromEvent) {
                    if (obj.time) {
                        //console.log(obj.time);
                        var start = moment("2017-01-04 " + startTime);
                        //console.log(start);
                        var now = moment("2017-01-04 " + obj.time);
                        //console.log(now);
                        var differenceInMs = now.diff(start, 's');
                        //console.log(differenceInMs);
                        var diffDividefour = parseInt((differenceInMs / 4));
                        //console.log(diffDividefour);
                        c3Chart.select([probandName], [diffDividefour]);

                        markerJumper(probandName, latFloatFromEvent, lngFloatFromEvent, diffDividefour);
                    }
                }
            });
            time = arrTrack[j];

    }

        return time;
    }

    function markerJumper(probandName, lat, lng,arrayIndex){

        var prob= getFreqs().filter(function (obj) {

            return obj.proband === probandName;
        });
        if(prob[0].freq[arrayIndex]) {
            var freq = prob[0].freq[arrayIndex].freq;
            console.log(freq);
            if (parseInt(freq) > 90) {

                var distance = setJump(lat, lng);
                //console.log(distance);
            }
        }
    }

    function setJump(lat,lng){
        var markers=getMarkers();
        var posLatlng =  new google.maps.LatLng(lat,lng);
        var distance=[];
        for(var i =0;i<markers.length;i++){
            var markerLatlng = markers[i].getPosition();
            distance[i]=parseInt(google.maps.geometry.spherical.computeDistanceBetween(posLatlng, markerLatlng));
            if (distance[i]<20){
                if (markers[i].getAnimation() == null) {
                    console.log(distance);
                    markers[i].setAnimation(google.maps.Animation.BOUNCE);

               } else {
                    markers[i].setAnimation(null);
                }
            }

        }
        return distance;
    }

    function stopJump() {

        var markers=getMarkers();
        for(var i =0;i<markers.length;i++){
            if (markers[i].getAnimation() !== null) {
                markers[i].setAnimation(null);

            }
        }

    }

</script>

</body>

</html>


