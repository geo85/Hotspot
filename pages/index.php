﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->

    <style>
        IMG.displayed {
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: 10px;
        }

    </style>
</head>

<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php"); ?>

    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Hotspot hunter v1.0<br>
                    <small>Geo-Visualisierungsprojekt 2016/17</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row header tittle-->

        <!-- row welcome text-->
        <div class="row">
            <!-- welcome text col-lg-12-->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Willkommen</strong>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <p>
                            Willkommen auf der Webseite Hotspot Hunter v1.0. Im Rahmen einer
                            Studentischen Projektarbeit an der Hochschule für Technik in Stuttgart und der Kooperation
                            der unterstützenden Dozenten, wurden gemessene Datensätze als Heatmap visualisiert. Zudem
                            werden aus den Daten Statistiken gewonnen. Hotspots, die als Aufnahme der Gefühlslage
                            fungieren,
                            dienen als Vergleich zwischen der persönlichen Empfindung und der gemessenen Herzfrequenzen.

                            Mit Hilfe der Webseite erhalten Sie Informationen darüber welche Orte in Ihrer Umgebung
                            für Sie selbst oder Ihrer Mitmenschen geeignet ist.<br><br>

                            Die Daten setzen sich aus Herzfrequenzen, Routen die aus Längen- und Breitengraden bestehen
                            und Hotspots mit deren Koordinaten zusammen. Alle Informationen der Daten wurden
                            ausgewählten
                            Probanden entnommen bzw. wurden mit Hilfe von Probanden gemessen. Eine sorgfältige
                            Ausarbeitung
                            der Daten führte zu der jetzigen Webseite.<br><br>

                            Die Ausschnitte die unten zu sehen sind, enthalten Beispiele über die links stehenden
                            Menüpunkte.
                            Durch das klicken auf ein Bild, wird dieses vergrößert und Sie erhalten von dort aus Zugang
                            auf die
                            entsprechende Seite.

                        </p>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.welcome tet col-lg-12 -->
        </div>
        <!-- /.row welcome text-->

        <!-- row headings -->
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Heatmap Visualisierung</strong>
                    </div>
                    <div class="panel-body" style="height: 190px;">
                        Die Visualisierung wurde in zwei Variationen ausgeführt.
                        Zur Auswahl stehen:
                        <h4><p class="text-primary"><a href="http://localhost/Hotspot/pages/googlemap.php">Google
                                    Maps</a></p></h4>
                        <ol>
                            <code>Heatmap der gesamten Route der Probanden</code>
                            <br>
                        </ol>
                        <h4><p style="color: forestgreen"><a href="http://localhost/Hotspot/pages/openstreetmap.php">Open
                                    Street Map</a></p></h4>
                        <ul>
                            <code>Heatmap der besuchten Hotspots</code>
                        </ul>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->

            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Hashtags</strong>
                    </div>
                    <div class="panel-body" style="height: 190px;">
                        <!-- Trigger the Modal -->
                        Die besuchten Hotspots der Probanden können im einzelnen betrachtet werden:

                        <img id="myImg" class="displayed" src="..\probandenData\images\bildHotspot.png"
                             width="90%" height=65%" data-toggle="modal" data-target="#picModal">

                        <div id="picModal" class="modal fade" role="article">
                            <div class="modal-dialog modal-lg">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Hotspot / Hashtag</h4>
                                    </div>
                                    <div class="modal-body">

                                        <img id="myImg" src="..\probandenData\images\bildHotspot.png" width="850"
                                             height="300">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"
                                                onclick="parent.location='http://localhost/Hotspot/pages/hashTagPage.php'">
                                            Gehe zu Hotspot / Hashtag
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->

            <!-- /.row headings -->

            <!-- row -->

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Latitude & Longitude Finder</strong>
                    </div>

                    <div class="panel-body" style="height: 190px;">
                        <!-- Trigger the Modal -->
                        Ermöglicht Ihnen die Koordinaten eines beliebingen Ortes auszugeben:

                        <img id="myImg" class="displayed" src="..\probandenData\images\location.png"
                             width="90%" height="65%" data-toggle="modal" data-target="#locModal">

                        <div id="locModal" class="modal fade" role="article">
                            <div class="modal-dialog modal-lg">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Latitude & Longitude</h4>
                                    </div>
                                    <div class="modal-body">

                                        <img id="myImg" src="..\probandenData\images\location.png" width="850"
                                             height="250">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"
                                                onclick="parent.location='http://localhost/Hotspot/pages/getLatLong.php'">
                                            Gehe zu Latitude & Longitude
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Charts</strong>
                    </div>
                    <div class="panel-body" style="height: 820px;">
                        <!-- Trigger the Modal -->
                        Die Charts beinhalten <strong>drei</strong> Ansichten.
                        Die erste zeigt Ihnen die persönlichen Bewertungen und Empfindungen der Probanden nach der Zeit
                        sortiert:

                        <img id="myImg" class="displayed" src="..\probandenData\images\appRating.png" width="80%"
                             height="20%" data-toggle="modal" data-target="#myModal">
                        <!-- Modal -->
                        <div id="myModal" class="modal fade" role="article">
                            <div class="modal-dialog modal-lg">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">App Rating</h4>
                                    </div>
                                    <div class="modal-body">
                                        <img id="myImg" src="..\probandenData\images\appRating.png" width="800"
                                             height="480">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"
                                                onclick="parent.location='http://localhost/Hotspot/pages/appRating.php'">
                                            Gehe zu App Rating
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="panel-body">
                            <!-- Trigger the Modal -->
                            <hr>
                            Im folgenden Popup wird der Laufweg zweiter Probanden und deren Herzfrequenz angezeigt:

                            <img id="myImg" class="displayed" src="..\probandenData\images\laufwege.png"
                                 width="90%" height="20%" data-toggle="modal" data-target="#laufModal">

                            <div id="laufModal" class="modal fade" role="article">
                                <div class="modal-dialog modal-lg">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Laufwege & Herzfrequenz</h4>
                                        </div>
                                        <div class="modal-body">
                                            <img id="myImg" src="..\probandenData\images\laufwege.png" width="800"
                                                 height="480">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                                    onclick="parent.location='http://localhost/Hotspot/pages/heartFreq.php'">
                                                Gehe zu Heart Frequenzy
                                            </button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <!-- Trigger the Modal -->
                            <hr>
                            Die dritte Ansicht zeigt die Heatmap anhand der Hotspots die besucht worden sind:

                            <img id="myImg" class="displayed" src="..\probandenData\images\hotspots.png"
                                 width="100%" height="20%" data-toggle="modal" data-target="#hotModal">

                            <div id="hotModal" class="modal fade" role="article">
                                <div class="modal-dialog modal-lg">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Herzfrequenzen an Hospots</h4>
                                        </div>
                                        <div class="modal-body">

                                            <img id="myImg" src="..\probandenData\images\hotspots.png" width="850"
                                                 height="300">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                                    onclick="parent.location='http://localhost/Hotspot/pages/heartFreqOS.php'">
                                                Gehe zu Heart Frequenzy / Hotspot
                                            </button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Schließen
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel -->
            </div>


            <!-- /.col-lg-4 -->
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Upload</strong>
                    </div>
                    <div class="panel-body" style="height: 820px;">

                        Neue Probanden können mit Hilfe eines Uploads in die Datenbank integriert werden.
                        Folgende Schritte sind dabei zu beachten:

                        <h4><p style="color: steelblue">Name des Probanden</p></h4>
                        <ul>
                            <li><strong>Eindeutige</strong> Namesgebung</li>
                            <li>
                                <ul>
                                    <li>proband+nummer</li>
                                    <li>Beispiel: <p style="color: red">proband19</p></li>
                                </ul>
                            </li>
                        </ul>
                        <h4><p style="color: steelblue">Herz-Freq Datei</p></h4>
                        <ul>
                            <li><strong>Gültige CSV Datei</strong> auswählen</li>
                            <li>
                                <ul>
                                    <li>Im Verzeichnis 01_Messdaten\[Probandenname\bpmbxblist.csv]</li>
                                    <li>Beispiel: <p style="color: red">movisens\movisens_Messdaten\Hotspots
                                            Stadtgarten\01_Messdaten\Proband2_ID11\bpmbxblist.csv</p></li>
                                </ul>
                            </li>
                        </ul>
                        <h4><p style="color: steelblue">Laufweg Datei</p></h4>
                        <ul>
                            <li><strong>Gültige KMZ Datei</strong> auswählen</li>
                            <li>
                                <ul>
                                    <li>ort_proband+nummer_datum+uhrzeit.kmz</li>
                                    <li>Beispiel: <p style="color: red">movisens\movisensxs_Hotspots
                                            Stadtgarten\Proband2_ID11\Stadtgarten
                                            Hotspots_Proband11_20160719T145430+0200.kmz</p></li>
                                </ul>
                            </li>
                        </ul>
                        <h4><p style="color: steelblue">movisensApp Datei</p></h4>
                        <ul>
                            <li><strong>Gültige CSV Datei</strong> auswählen</li>
                            <li>
                                <ul>
                                    <li>ort_proband+nummer_datum+uhrzeit.kmz</li>
                                    <li>Beispiel: <p style="color: red">movisens\movisensxs_Hotspots
                                            Stadtgarten\Proband2_ID11\Stadtgarten
                                            Hotspots_Proband11_20160719T150210+0200.csv</p></li>
                                </ul>
                            </li>
                        </ul>
                        <h4><p style="color: steelblue">Hotspot Bilder</p></h4>
                        <ul>
                            <li>Gewünschte <strong>Fotos</strong> auswählen</li>

                            <ul>
                                <li>Das Auswahlverzeichnis aus dem Beispiel entnehmen!</li>
                                <li>Beispiel:</li>
                                <ul>
                                    <li>movisens\movisensxs_Hotspots Stadtgarten\Proband2_ID11\<p style="color: red">
                                            1465996918559.jpg</p></li>
                                    <li>movisens\movisensxs_Hotspots Stadtgarten\Proband2_ID11\<p style="color: red">
                                            1465997285618.jpg</p></li>

                                </ul>
                            </ul>
                            [Es könnnen mehrere Fotos ausgewählt werden. Halten Sie dazu die Taste <strong>STRG</strong>
                            bei der Auswahl gedrückt!]
                        </ul>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <div class="row">
            <!-- row footer -->
            <div>
                <?php include_once("footer.php") ?>
            </div>
            <!-- /.row footer -->
        </div>
        <!-- /.row -->
    </div>
    <!-- row -->

    <!-- /.row -->

    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>


</body>

</html>
