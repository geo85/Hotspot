﻿<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <style>
        #map-container {
            height: 350px
        }

        #hash-names {
            z-index: 10;
            height: 350px;
            max-height: 350px;
            overflow-y: scroll;
        }

        #list-view {
        }

        #list-view h7 {
            font: 400 20px/1.5 Helvetica, Verdana, sans-serif;
            margin: 0;
            padding: inherit;
        }

        #list-view ul {
            list-style-type: none;
            margin: 0;
            padding: inherit;
        }

        #list-view li {
            font: 200 10px/1.5 Helvetica, Verdana, sans-serif;
            border-bottom: 1px solid #ccc;
        }

        #list-view li:last-child {
            border: none;
        }

        #list-view li a {
            text-decoration: none;
            color: #000;

            -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
            -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
            -o-transition: font-size 0.3s ease, background-color 0.3s ease;
            -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
            transition: font-size 0.3s ease, background-color 0.3s ease;
            display: block;
            width: 200px;
        }

        #list-view li a:hover {
            font-size: 20px;
            background: #f6f6f6;
        }

    </style>
    <title>Hotspot hunter v1.0</title>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    -->
    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<script src="../js/googleMap.js"></script>
<div id="wrapper">

    <!-- Load the main navigation menu here -->
    <?php include_once("mainMenu.php"); ?>

    <!-- / .main navigation menu  -->

    <div id="page-wrapper">

        <!-- row header tittle -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="headi">Hotspot / Hashtag</small></h3>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row header tittle-->

        <!-- row welcome text-->
        <div class="row">

            <div class="panel panel-default">
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Hotspot Bild
                        </div>
                        <div class="panel-body">
                            <div id="bild-container"></div>
                            <img id="hotspotBild" style="max-width: 100%;max-height: 350px;height: 350px;"/>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.welcome tet col-lg-6 -->
                <div class="col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Hotspot Position
                        </div>
                        <div class="panel-body">
                            <div id="map-container"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.welcome tet col-lg-6 -->
            </div>
            <!-- /.panel -->


        </div>
        <!-- /.row welcome text-->

        <!-- row footer -->
        <div class="row">
            <?php include_once("footer.php") ?>
        </div>
        <!-- /.row footer -->
    </div>
    <!-- /#page-wrapper -->


</div>
<!-- /#wrapper -->

<!-- Bootstrap Core JavaScript -->
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<script type="text/javascript">


    function showMarker(lat, lng, pic, bewertung, hashtag) {
        //console.log("lat: " + lat + " ,lng: " + lng + " ,pic: " + pic + " ,bewertung: " + bewertung);
        var markerPos = new google.maps.LatLng(lat, lng);

        var infowindow = new google.maps.InfoWindow({
            content: hashtag
        });

        var marker = new google.maps.Marker({
            position: markerPos,
        });
        marker.setIcon("../images/marker_" + bewertung + ".png");
        var latLng = marker.getPosition(); // returns LatLng object
        map.setCenter(latLng);
        map.setZoom(17);
        marker.setMap(map);

        var img = document.createElement("img");
        var src = '../probandenData/images/' + pic;
        //console.log(img.src);
        document.getElementById("hotspotBild").src = src;
        marker.addListener('mouseover', function () {
            infowindow.open(map, marker);
        });
        marker.addListener('mouseout', function () {
            infowindow.close();
        });

    }


</script>
</body>

</html>
