<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

require_once '../logic/PolylineEncoder.php';
$stringToDecode = "_p~iF~ps|U_ulLnnqC_mqNvxq`@";
echo "String to decode :\t$stringToDecode\n";
$decodedPoints = PolylineEncoder::decodeValue($stringToDecode);
print_r($decodedPoints);